<div class="cell markdown">

# Automatic Search of Rectangle Attacks on Feistel Ciphers: Application to WARP

**NOTE:** The code starting with an `!` is bash call.

## Dependencies

The dependencies for the project are the following:

-   Java 11.0.12
-   Gradle 6.8
-   Minizinc 2.5.5
-   Picat 3.2

We provide a docker image that contains all the dependencies:

</div>

<div class="cell code" execution_count="1">

``` sh
docker build --rm -f Dockerfile -t tosc:LMR23 .
```

<div class="output stream stdout">

    Sending build context to Docker daemon  67.35MB
    Step 1/20 : FROM ubuntu:20.04
     ---> 53df61775e88
    Step 2/20 : ARG USER_UID="1000"
     ---> Using cache
     ---> 57e18887c59c
    Step 3/20 : ARG USER_GID="1000"
     ---> Using cache
     ---> 70a390b772d4
    Step 4/20 : ARG USER_NAME="default"
     ---> Using cache
     ---> 683e6d51680b
    Step 5/20 : RUN groupadd -g $USER_GID $USER_NAME    && useradd -m -g $USER_GID -u $USER_UID $USER_NAME
     ---> Using cache
     ---> a5809f22823c
    Step 6/20 : RUN apt-get update    && apt-get install -y zip unzip curl
     ---> Using cache
     ---> 8acbd275c911
    Step 7/20 : USER $USER_UID:$USER_GID
     ---> Using cache
     ---> 9f2861194085
    Step 8/20 : RUN curl -s "https://get.sdkman.io" | bash
     ---> Using cache
     ---> 3df8e6898f3c
    Step 9/20 : ARG JAVA_VERSION="11.0.12-open"
     ---> Using cache
     ---> 182bde099b12
    Step 10/20 : ARG GRADLE_VERSION="6.8"
     ---> Using cache
     ---> 0c0a59dcef75
    Step 11/20 : RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh    && sdk install java $JAVA_VERSION    && sdk install gradle $GRADLE_VERSION"
     ---> Using cache
     ---> cc485b9062e2
    Step 12/20 : ENV GRADLE_HOME="/home/default/.sdkman/candidates/gradle/current"
     ---> Using cache
     ---> 20c2fd26c9ae
    Step 13/20 : ENV JAVA_HOME="/home/default/.sdkman/candidates/java/current"
     ---> Using cache
     ---> 32322d1ec08a
    Step 14/20 : ARG MINIZINC_VERSION="2.5.5"
     ---> Using cache
     ---> 2195746b3670
    Step 15/20 : RUN cd /home/default    && curl -L "https://github.com/MiniZinc/MiniZincIDE/releases/download/${MINIZINC_VERSION}/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz" -o MiniZinc.tgz    && tar -xzf MiniZinc.tgz    && rm MiniZinc.tgz
     ---> Using cache
     ---> 432e9898ae1e
    Step 16/20 : ENV MINIZINC_HOME="/home/default/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64"
     ---> Using cache
     ---> 0b9a7035b582
    Step 17/20 : ARG PICAT_VERSION="30_5"
     ---> Using cache
     ---> c7f45976f79a
    Step 18/20 : RUN cd /home/default    && curl -L "http://picat-lang.org/download/picat${PICAT_VERSION}_linux64.tar.gz" -o Picat.tgz    && tar -xzf Picat.tgz    && rm Picat.tgz    && cd Picat/lib    && curl -L http://picat-lang.org/flatzinc/fzn_picat_sat.pi -o fzn_picat_sat.pi    && curl -L http://picat-lang.org/flatzinc/fzn_parser.pi -o fzn_parser.pi    && curl -L http://picat-lang.org/flatzinc/fzn_tokenizer.pi -o fzn_tokenizer.pi
     ---> Using cache
     ---> e95c61c574e0
    Step 19/20 : ENV PICAT_HOME="/home/default/Picat"
     ---> Using cache
     ---> 02c7e2575507
    Step 20/20 : ENV PATH="$PICAT_HOME:$MINIZINC_HOME/bin:$GRADLE_HOME/bin:$JAVA_HOME/bin:$PATH"
     ---> Using cache
     ---> 8100d11d79bf
    Successfully built 8100d11d79bf
    Successfully tagged tosc:LMR23

</div>

</div>

<div class="cell markdown">

To use the docker image it is recommended to use an alias:

</div>

<div class="cell code" execution_count="2">

``` sh
alias warpenv='docker run -v $(pwd):/home/app:z  -w /home/app --user $(id -u):$(id -g) tosc:LMR23 '
```

</div>

<div class="cell markdown">

The `config.txt` file is configured to use the docker image. If you
already have the dependencies installed you can change the configuration
file to point at your binaries.

## Usage

### Getting the code

The get the code you have to copy the source code from the git
repository and initialize the submodules.

</div>

<div class="cell code">

``` sh
git clone git@gitlab.inria.fr:lrouquet/boomerang-warp-fse-23.git
git submodule update --init
```

</div>

<div class="cell markdown">

### Compilation

To compile the `.jar` you can use either `./gradlew jar` if you want to
use the gradle wrapper or the docker env with the following command:

</div>

<div class="cell code" execution_count="3">

``` sh
warpenv gradle jar
```

<div class="output stream stdout">


    Welcome to Gradle 6.8!

    Here are the highlights of this release:
     - Faster Kotlin DSL script compilation
     - Vendor selection for Java toolchains
     - Convenient execution of tasks in composite builds
     - Consistent dependency resolution

    For more details see https://docs.gradle.org/6.8/release-notes.html

    Starting a Gradle Daemon (subsequent builds will be faster)
    > Task :phd-core:compileKotlin UP-TO-DATE
    > Task :phd-core:compileJava NO-SOURCE
    > Task :phd-core:processResources UP-TO-DATE
    > Task :phd-core:classes UP-TO-DATE
    > Task :phd-core:inspectClassesForKotlinIC UP-TO-DATE
    > Task :phd-core:jar UP-TO-DATE
    > Task :phd-infrastructure:compileKotlin UP-TO-DATE
    > Task :phd-infrastructure:compileJava NO-SOURCE
    > Task :phd-infrastructure:processResources NO-SOURCE
    > Task :phd-infrastructure:classes UP-TO-DATE
    > Task :phd-infrastructure:inspectClassesForKotlinIC UP-TO-DATE
    > Task :phd-infrastructure:jar UP-TO-DATE
    > Task :compileKotlin UP-TO-DATE
    > Task :compileJava UP-TO-DATE
    > Task :processResources UP-TO-DATE
    > Task :classes UP-TO-DATE
    > Task :inspectClassesForKotlinIC UP-TO-DATE
    > Task :jar UP-TO-DATE

    BUILD SUCCESSFUL in 39s
    12 actionable tasks: 12 up-to-date

</div>

</div>

<div class="cell markdown">

### Step-1 Opt

The optimization search for the truncated boomerang distinguisher can be
done with the following command:

</div>

<div class="cell code" execution_count="4">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppIter\
  Step1Solver=Picat
```

<div class="output stream stdout">

    Step1Solver = Picat
    Nr = 3

    ObjStep1: 2^{-0}, time: 2.31s
    Nr = 4

    ObjStep1: 2^{-0}, time: 4.56s
    Nr = 5

    ObjStep1: 2^{-0}, time: 10.58s
    Nr = 6

    ObjStep1: 2^{-0}, time: 16.94s
    Nr = 7
    ^C

</div>

</div>

<div class="cell markdown">

### Step-2 Opt

To compute the distinguisher probability you can two commands:

-   this command allows to compute the Step-1 Opt then the Step-2 Opt:

</div>

<div class="cell code" execution_count="5">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.App\
  Nr=10 SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">

    Time to solve Step1Opt = 20.39s
    Searching new Step1 solution for with UB = 2^{-2}, LB = 2^{-2147483647}
    Step 1 solution found (2.31s)

    Step 2 solution found. Probability = 2^{-2} (0.02s)
    Exact proba: 2^{-2.00}
    No more Step 2 solution (0.00s)
    Step1OptTime	Step1EnumTime	Step2OptTime	Best proba
    20.47s	2.31s	0.09s	2^{-2}

</div>

</div>

<div class="cell markdown">

-   this command allows to compute directly the Step-2 Opt when the
    Step-1 Opt value is already known:

</div>

<div class="cell code" execution_count="6">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.App\
  Nr=10 ObjStep1=2 SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">

    Searching new Step1 solution for with UB = 2^{-2}, LB = 2^{-2147483647}
    Step 1 solution found (2.30s)

    Step 2 solution found. Probability = 2^{-2} (0.03s)
    Exact proba: 2^{-2.00}
    No more Step 2 solution (0.00s)
    Step1OptTime	Step1EnumTime	Step2OptTime	Best proba
    0.00s	2.38s	0.10s	2^{-2}

</div>

</div>

<div class="cell markdown">

`Nr` is the number of rounds of the distinguisher. `ObjStep1 (optional)`
is the upper bound probability of the truncated distinguisher.

The `.tex` solutions for the Step-2 are saved in
`solutions/step2/warp-{Nr}.tex`.

</div>

<div class="cell markdown">

### Step-2 with attack

To compute the attack complexity you can use the following command:

</div>

<div class="cell code" execution_count="7">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppWithStep3\
  Eb=1 Ed=10 Ef=1 SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">

     LOG   	User parameters: {Ef=1, Eb=1, cluster_size_approximation=0, Ed=10}
     LOG   	Time to solve Step1Opt = 32.01s
     LOG   	Searching new Step1 solution for with UB = 2^{2147483647}, LB = 2^{62}
     LOG   	Step 1 solution found (0.62s)

     LOG   	Step 2 solution found
     LOG   	Probability = 2^{-2} (0.03s)
     LOG   	Data complexity = 2^{66}
     LOG   	Time complexity = 2^{62}
     LOG   	Attack III stage 1 = 2^{62}
     LOG   	Attack III stage 2 = 2^{-118}
     LOG   	rb = 12
     LOG   	rf = 4
     LOG   	mb = 0
     LOG   	No more Step 2 solution (0.00s)
     LOG   	Step1OptTime	Step1EnumTime	Step2OptTime	Best time complexity
     LOG   	32.08s	0.62s	0.13s	2^{62}

</div>

</div>

<div class="cell markdown">

`Eb` is the number of rounds for the key recovery before the
distinguisher. `Ed` is the number of rounds of the distinguisher. `Ef`
is the number of rounds for the key recovery after the distinguisher.

The `.tex` solutions for the Step-2 are saved in
`solutions/step2-time-complexity/warp-{Eb}-{Ed}-{Ef}.tex`.

</div>

<div class="cell markdown">

### Clustering

#### For distinguisher result

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppWithStep3\
  Mode=Distinguisher Selection=[AUTO | MANUAL]\
  Nr= [Start= End=]\
  ProbaOpt= ProbaLimit=\
  SolverStep1Opt=Picat SolverStep1Enum=Picat
```

**NOTE:** The instances to be clustered **must** be computed before the
cluster computation by using `warp.App` or `warp.AppWithStep3`.

`Mode` indicates if you want to use a `Distinguisher` solution
(`warp.App` result) or an `Attack` solution (`warp.AppwithStep3`). `Nr`
is the number of rounds of the distinguisher. `Selection` can be either
`AUTO` or `MANUAL`. In `AUTO` mode the algorithm select automatically
where to start and stop the clustering computation in the distinguisher.
The `MANUAL` mode allows to select manually where to start and stop the
clustering computation. The `MANUAL` mode requires to additionnal
parameters which are `Start` end `End`. `ProbaOpt` is the optimal
probability for the distinguisher. `ProbaLimit` is a limit that filter
the solutions for which the probability is lower.

Here an example with manual selection of the Start and the End of the
cluster:

</div>

<div class="cell code" execution_count="8">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppCluster\
  Mode=Distinguisher Selection=MANUAL\
  Nr=10 Start=0 End=9\
  ProbaOpt=2 ProbaLimit=12\
  SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">


     LOG   	New Step1 sol
     LOG   	r = 2^{-2}
     LOG   	2^{0} 0.01s
     LOG   	2^{-2}
     LOG   	Model[Model-0], 1 Solutions, Resolution time 0.009s, 1 Nodes (108.3 n/s), 0 Backtracks, 0 Backjumps, 0 Fails, 0 Restarts
     LOG   	2^{-2}, count = 1

</div>

</div>

<div class="cell markdown">

The same example, but with an automatic selection:

</div>

<div class="cell code" execution_count="9">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppCluster\
  Mode=Distinguisher Selection=AUTO\
  Nr=10\
  ProbaOpt=2 ProbaLimit=12\
  SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">


     LOG   	New Step1 sol
     LOG   	r = 2^{-2}
     LOG   	2^{0} 0.01s
     LOG   	2^{-2}
     LOG   	Model[Model-1], 1 Solutions, Resolution time 0.011s, 1 Nodes (91.2 n/s), 0 Backtracks, 0 Backjumps, 0 Fails, 0 Restarts
     LOG   	2^{-2}, count = 1

</div>

</div>

<div class="cell markdown">

#### For attack result

The clustering computation is done in the same way on attack solution
but requires to indicates the `Eb`, `Ed` and `Ef` parameters instead of
`Nr`.

**NOTE:** The round index are from the top of the attack and not from
the top of the distinguisher, e.g. to set the start position at the
first round of the distinguisher you should have `Eb=x Start=x`.

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppWithStep3\
  Mode=Attack Selection=[AUTO | MANUAL]\
  Eb= Ed= Ef= [Start= End=]\
  ProbaOpt= ProbaLimit=\
  SolverStep1Opt=Picat SolverStep1Enum=Picat
```

Example of usage:

</div>

<div class="cell code" execution_count="10">

``` sh
warpenv java -cp build/libs/boomerang-warp-1.0-SNAPSHOT.jar warp.AppWithStep3\
 Mode=Attack Selection=MANUAL\
 Eb=1 Ed=10 Ef=1 Start=1 End=11\
 ProbaOpt=2 ProbaLimit=12\
 SolverStep1Opt=Picat SolverStep1Enum=Picat
```

<div class="output stream stdout">

     LOG   	User parameters: {Ef=1, Eb=1, cluster_size_approximation=0, Ed=10}
     LOG   	Time to solve Step1Opt = 31.83s
     LOG   	Searching new Step1 solution for with UB = 2^{2147483647}, LB = 2^{62}
     LOG   	Step 1 solution found (0.54s)

     LOG   	Step 2 solution found
     LOG   	Probability = 2^{-2} (0.03s)
     LOG   	Data complexity = 2^{66}
     LOG   	Time complexity = 2^{62}
     LOG   	Attack III stage 1 = 2^{62}
     LOG   	Attack III stage 2 = 2^{-118}
     LOG   	rb = 12
     LOG   	rf = 4
     LOG   	mb = 0
     LOG   	No more Step 2 solution (0.00s)
     LOG   	Step1OptTime	Step1EnumTime	Step2OptTime	Best time complexity
     LOG   	31.90s	0.54s	0.14s	2^{62}

</div>

</div>
