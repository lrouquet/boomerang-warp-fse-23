package sboxtransition

import com.github.phd.core.cryptography.attacks.boomerang.util.Expr
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable
import warp.FeistelSboxTables

class FeistelDDTTransition(val α: Expr, val β: Expr) : FeistelSboxTransition {

    override val variables by lazy { (α.variables + β.variables).toSet() }

    override val isInteresting = true

    override fun imposeVariable(variable: Variable, value: Int) =
        FeistelDDTTransition(
            α.imposeVariable(variable, value),
            β.imposeVariable(variable, value)
        )

    override fun getCstProba(tables: FeistelSboxTables) = tables.ddtProba(α.ensureCst(), β.ensureCst())

    override fun getVariableDomain(variable: Variable, tables: FeistelSboxTables) =
        outputDiffDDT(α, β, tables, variable)
            ?: inputDiffsDDT(β, α, tables, variable)
            ?: tables.values


    override fun toString() = "DDT($α, $β)"
}