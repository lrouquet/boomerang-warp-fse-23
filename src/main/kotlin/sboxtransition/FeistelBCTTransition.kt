package sboxtransition

import com.github.phd.core.cryptography.attacks.boomerang.SPNSboxTables
import com.github.phd.core.cryptography.attacks.boomerang.sboxtransition.SPNSboxTransition
import com.github.phd.core.cryptography.attacks.boomerang.util.Expr
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable
import warp.FeistelSboxTables

class FeistelBCTTransition(val γ: Expr, val δ: Expr) : FeistelSboxTransition {

    override val variables by lazy { (γ.variables + δ.variables).toSet() }

    override val isInteresting = !γ.isZero && !δ.isZero

    override fun imposeVariable(variable: Variable, value: Int) =
        FeistelBCTTransition(
            γ.imposeVariable(variable, value),
            δ.imposeVariable(variable, value)
        )

    override fun getCstProba(tables: FeistelSboxTables) = tables.fbctProba(γ.ensureCst(), δ.ensureCst())

    override fun getVariableDomain(variable: Variable, tables: FeistelSboxTables) =
        outputDiffBCT(γ, δ, tables, variable)
            ?: inputDiffsBCT(δ, γ, tables, variable)
            ?: tables.values

    override fun toString() = "BCT($γ, $δ)"
}