package sboxtransition

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable
import warp.FeistelSboxTables

class FeistelFBETransition(val γ: XorExpr, val θ: XorExpr, val λ: XorExpr, val δ: XorExpr) : FeistelSboxTransition {

    companion object {
        @JvmStatic
        fun transitionOf(γ: XorExpr, θ: XorExpr?, λ: XorExpr?, δ: XorExpr): FeistelSboxTransition {
            return when {
               θ == null -> FeistelBDTTransition.transitionOf(γ, λ, δ)
               λ == null -> FeistelBDTTransition.transitionOf(γ, θ, δ)
               γ.isZero && θ.isZero -> FeistelDDTTransition(λ, δ)
               λ.isZero && δ.isZero -> FeistelDDTTransition(γ, θ)
               γ.isZero && θ.isZero && λ.isZero || δ.isZero -> throw IllegalStateException("")
                else -> FeistelFBETransition(γ, θ, λ, δ)
            }
        }
    }

    override val variables by lazy { (γ.variables + θ.variables + λ.variables + δ.variables).toSet() }
    override val isInteresting = true

    override fun imposeVariable(variable: Variable, value: Int) =
        FeistelFBETransition(
            γ.imposeVariable(variable, value),
            θ.imposeVariable(variable, value),
            λ.imposeVariable(variable, value),
            δ.imposeVariable(variable, value)
        )

    override fun getCstProba(tables: FeistelSboxTables) =
        tables.fbetProba(γ.ensureCst(), θ.ensureCst(), λ.ensureCst(), δ.ensureCst())

    override fun getVariableDomain(variable: Variable, tables: FeistelSboxTables) =
        outputDiffDDT(γ, θ, tables, variable)
            ?: inputDiffsDDT(θ, γ, tables, variable)
            ?: outputDiffDDT(λ, δ, tables, variable)
            ?: inputDiffsDDT(δ, λ, tables, variable)
            ?: tables.values

    override fun toString() = "FBCT($γ, $θ, $λ, $δ)"

}