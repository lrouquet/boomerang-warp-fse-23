package sboxtransition

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable
import warp.FeistelSboxTables

class FeistelBDTTransition(val γ: XorExpr, val λ: XorExpr, val δ: XorExpr) : FeistelSboxTransition {

    companion object {
        @JvmStatic
        fun transitionOf(gamma: XorExpr, lambda: XorExpr?, δ: XorExpr): FeistelSboxTransition {
            return when {
                lambda == null -> FeistelBCTTransition(gamma, δ)
                gamma.isZero -> FeistelDDTTransition(lambda, δ)
                else -> FeistelBDTTransition(gamma, lambda, δ)
            }
        }
    }

    override val variables by lazy {
        (γ.variables + λ.variables + δ.variables).toSet()
    }

    override val isInteresting = true

    override fun imposeVariable(variable: Variable, value: Int) =
        FeistelBDTTransition(
            γ.imposeVariable(variable, value),
            λ.imposeVariable(variable, value),
            δ.imposeVariable(variable, value)
        )

    override fun getCstProba(tables: FeistelSboxTables) = tables.fbdtProba(γ.ensureCst(), λ.ensureCst(), δ.ensureCst())

    override fun getVariableDomain(variable: Variable, tables: FeistelSboxTables) =
        outputDiffDDT(λ, δ, tables, variable)
            ?: inputDiffsDDT(δ, λ, tables, variable)
            ?: tables.values

    override fun toString() = "LBCT($γ, $λ, $δ)"
}