package sandwichproba.util;

import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;
import org.javatuples.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A class to have the representation of the transition dependencies as a graph.
 * Is used to generate a tree to know in which order to instanciate the variables to compute the formula
 *
 * @author Mathieu Vavrille
 */
public class Graph {
  private final Set<Variable> allVars;
  private final Map<Variable, Set<Variable>> adjMatrix;

  private Graph(final Set<Variable> allVars, final Map<Variable, Set<Variable>> adjMatrix) {
    this.allVars = allVars;
    this.adjMatrix = adjMatrix;
  }

  /** From a list of hyperedges, create a graph */
  public static Graph ofHypergraph(final List<Set<Variable>> hypergraph) {
    final Set<Variable> allVars = hypergraph.stream()
      .flatMap(Collection::stream)
      .collect(Collectors.toSet());
    return new Graph(allVars,
                     allVars.stream()
                     .collect(Collectors.toMap(var -> var, var -> getNeighbors(var, hypergraph))));
  }

  private static Set<Variable> getNeighbors(final Variable variable, final List<Set<Variable>> hypergraph) {
    return hypergraph.stream()
      .filter(hyperedge -> hyperedge.contains(variable))
      .flatMap(hyperedge -> hyperedge.stream()
               .filter(var -> var != variable))
      .collect(Collectors.toSet());
  }

  // TODO Does NOT return the maximum independent set, but an approximation
  private Set<Variable> getMaxIndependentSet() {
    final List<Variable> orderedByDegree = allVars.stream()
      .map(var -> new Pair<Integer, Variable>(adjMatrix.get(var).size(), var))
      .sorted()
      .map(Pair::getValue1)
      .collect(Collectors.toList());
    final Set<Variable> independentSet = new HashSet<Variable>();
    for (int i = 0; i < orderedByDegree.size(); i++) {
      final int currentId = i;
      if (independentSet.stream().noneMatch(var -> adjMatrix.get(var).contains(orderedByDegree.get(currentId)))) {
        independentSet.add(orderedByDegree.get(currentId));
      }
    }
    return independentSet;
  }

  /** Generate an affectation tree.
   * The generated tree is consistent with the hyper edges, meaning that there is no edge spanning on multiple subtrees
   */
  public TreeVariable getAffectationTree() {
    Set<Variable> unaffectedVariables = new HashSet<Variable>(allVars);
    List<Variable> independentSet = new ArrayList<Variable>(getMaxIndependentSet());
    unaffectedVariables.removeAll(independentSet);
    
    // Initialize the forest
    List<Set<Variable>> increasedIndependentSet = independentSet.stream()
      .map(var -> new HashSet<Variable>(Collections.singleton(var)))
      .collect(Collectors.toList());
    Set<Variable> newlyAddedVariables = new HashSet<Variable>();
    for (Variable unaffectedVar : unaffectedVariables) {
      List<Set<Variable>> adjacentIndependentSets = increasedIndependentSet.stream()
        .filter(set -> set.stream().anyMatch(var -> adjMatrix.get(var).contains(unaffectedVar)))
        .collect(Collectors.toList());
      if (adjacentIndependentSets.size() == 0) {
        throw new IllegalStateException("The independent set could be bigger");
      }
      else if (adjacentIndependentSets.size() == 1) {
        newlyAddedVariables.add(unaffectedVar);
        adjacentIndependentSets.get(0).add(unaffectedVar);
      }
    }
    unaffectedVariables.removeAll(newlyAddedVariables);
    List<TreeVariable> forest = increasedIndependentSet.stream().map(TreeVariable::new).collect(Collectors.toList());
    
    while (unaffectedVariables.size() != 0) {
      final List<TreeVariable> finalTempoForest = new ArrayList<TreeVariable>(forest);
      Map<Variable,Set<Integer>> varsLinkedToTrees = unaffectedVariables.stream()
        .collect(Collectors.toMap(var -> var,
                                  var -> IntStream.range(0,finalTempoForest.size()).boxed()
                                  .filter(index -> new HashSet<Variable>(adjMatrix.get(var)).removeAll(finalTempoForest.get(index).getVariables()))
                                  .collect(Collectors.toSet())));
      Variable bestVar = unaffectedVariables.stream()
      .map(var -> new Pair<Integer, Variable>(adjMatrix.get(var).size(), var))
      .sorted()
      .map(Pair::getValue1)
      .collect(Collectors.toList())
        .get(0);
      Set<Integer> bestSubForest = varsLinkedToTrees.get(bestVar);
      Set<Variable> equallyBestVariables = unaffectedVariables.stream()
        .filter(var -> varsLinkedToTrees.get(var).equals(bestSubForest))
        .collect(Collectors.toSet());
      List<TreeVariable> newForest = IntStream.range(0,finalTempoForest.size()).boxed()
        .filter(index -> !bestSubForest.contains(index))
        .map(finalTempoForest::get)
        .collect(Collectors.toList());
      List<TreeVariable> mergedForest = IntStream.range(0,finalTempoForest.size()).boxed()
        .filter(bestSubForest::contains)
        .map(finalTempoForest::get)
        .collect(Collectors.toList());
      newForest.add(new TreeVariable(equallyBestVariables, mergedForest));
      forest = newForest;
      unaffectedVariables.removeAll(equallyBestVariables);
    }
    if (forest.size() == 1)
      return forest.get(0);
    else
      return new TreeVariable(forest);
  }

  public String toString() {
    return adjMatrix.toString();
  }
  
}
