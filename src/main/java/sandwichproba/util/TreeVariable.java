package sandwichproba.util;

import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A tree class to represent the order of affectation of variables to compute the formula
 *
 * @author Mathieu Vavrille
 */
public class TreeVariable {
  public final Set<Variable> levelVariables;
  public final List<TreeVariable> subTrees;
  private final Set<Variable> allVars;

  public TreeVariable(final Set<Variable> levelVariables, final List<TreeVariable> subTrees) {
    this.levelVariables = levelVariables;
    this.subTrees = subTrees;
    this.allVars = new HashSet<Variable>(levelVariables);
    subTrees.stream()
      .forEach(tree -> allVars.addAll(tree.getVariables()));
  }

  public TreeVariable(final Set<Variable> levelVariables) {
    this(levelVariables, new ArrayList<TreeVariable>());
  }
  
  public TreeVariable(final List<TreeVariable> subTrees) {
    this(new HashSet<Variable>(), subTrees);
  }
  
  public TreeVariable() {
    this(new HashSet<Variable>(), new ArrayList<TreeVariable>());
  }

  public void removeLevelVariable(final Variable variable) {
    levelVariables.remove(variable);
  }

  public void addLevelVariable(final Variable variable) {
    levelVariables.add(variable);
  }

  public Set<Variable> getVariables() {
    return allVars;
  }

  public Set<Variable> getLevelVariables() {
    return levelVariables;
  }

  public List<TreeVariable> getSubTrees() {
    return subTrees;
  }

  public boolean isLeaf() {
    return subTrees.isEmpty();
  }

  public boolean hasLevelVariables() {
    return !levelVariables.isEmpty();
  }

  public String toString() {
    return "T(" + levelVariables.toString() + "," + subTrees.toString() + ")";
  }

  public void preetyPrint() {
    preetyPrint(0);
  }

  private void preetyPrint(final int tabValue) {
    System.out.println("    ".repeat(tabValue) + levelVariables);
    subTrees.stream()
      .forEach(subTree -> subTree.preetyPrint(tabValue+1));
  }
}
