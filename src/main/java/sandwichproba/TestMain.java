package sandwichproba;

public class TestMain {
  
  public static void main(String[] args) {
    final int tk = 1;
    final int r = 15;
    final int bits = 64;
    
    int nbRounds = -1;
    int[][] inputDifference = {{}};
    int[][][] upperKeyDifference = {{{}}};
    int[][] outputDifference = {{}};
    int[][][] lowerKeyDifference = {{{}}};
    if (tk == 0 && r == 14 && bits == 64) {
      nbRounds = 3;
      inputDifference = new int[][]{{1,0,0,2},{3,0,0,0},{0,4,0,0},{5,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{6,0,0,0},{0,0,0,0},{0,0,0,0},{0,7,0,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    if (tk == 0 && r == 13 && bits == 128) {
      nbRounds = 2;
      inputDifference = new int[][]{{32,32,32,32},{0,32,0,0},{0,0,32,32},{0,32,32,32}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,0,2},{203,202,201,204},{0,201,204,203},{0,204,203,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    if (tk == 0 && r == 14 && bits == 128) {
      nbRounds = 2;
      inputDifference = new int[][]{{32,32,32,32},{0,32,0,0},{0,0,32,32},{0,32,32,32}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,0,2},{203,202,201,204},{0,201,204,203},{0,204,203,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 14 && bits == 64) {
      nbRounds = 5;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,2,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,2,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,2,0,0}}};
      outputDifference = new int[][]{{0,0,1,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
      lowerKeyDifference = new int[][][]{{{0,1,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{1,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 15 && bits == 64) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{2,0,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,2,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{10,0,0,0},{2,0,0,0},{0,0,0,0},{0,0,0,0}};
      lowerKeyDifference = new int[][][]{{{0,5,0,0},{0,0,0,0}},{{0,10,2,0},{0,0,0,0}},{{5,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 14 && bits == 128) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,2,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,2,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,255}};
      lowerKeyDifference = new int[][][]{{{0,16,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{16,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 15 && bits == 128) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,8,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,8,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{5,0,0,0},{1,0,0,0},{0,0,0,0},{0,0,0,0}};
      lowerKeyDifference = new int[][][]{{{0,5,0,0},{0,0,0,0}},{{0,5,1,0},{0,0,0,0}},{{5,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 16 && bits == 128) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{16,0,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,16,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{5,0,0,0},{1,0,0,0},{0,0,0,0},{0,0,0,0}};
      lowerKeyDifference = new int[][][]{{{0,5,0,0},{0,0,0,0}},{{0,5,1,0},{0,0,0,0}},{{5,0,0,0},{0,0,0,0}}};
    }
    if (tk == 1 && r == 17 && bits == 128) {
      nbRounds = 3;
      inputDifference = new int[][]{{0,0,1,0},{0,0,0,0},{0,0,0,5},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,5,0,0}},{{0,0,0,0},{0,5,0,1}}};
      outputDifference = new int[][]{{0,0,0,0},{0,2,0,32},{0,0,32,0},{0,32,0,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,2,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    if (tk == 2 && r == 17 && bits == 64) {
      nbRounds = 5;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,11,0,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,12,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,3,0,0}}};
      outputDifference = new int[][]{{0,0,6,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
      lowerKeyDifference = new int[][][]{{{0,12,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{5,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 2 && r == 18 && bits == 64) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,2,0,0},{0,0,0,0},{0,2,0,0},{0,2,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,13,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,14},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,10,0,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,12}},{{0,0,0,0},{0,0,0,0}},{{0,5,0,0},{0,0,0,0}}};
    }
    else if (tk == 2 && r == 19 && bits == 64) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,2},{0,0,0,0},{0,0,0,2},{0,0,0,2}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,13,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,14,0,0}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,2}};
      lowerKeyDifference = new int[][][]{{{0,12,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{5,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 2 && r == 18 && bits == 128) {
      nbRounds = 3;
      inputDifference = new int[][]{{0,255,0,0},{0,0,0,0},{0,255,0,0},{0,255,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,12,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,101}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{129,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 2 && r == 19 && bits == 128) {
      nbRounds = 2;
      inputDifference = new int[][]{{103,101,0,102},{0,101,0,0},{0,0,12,102},{0,101,0,102}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,24,0,0},{201,0,0,0},{0,0,0,201},{0,0,201,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 2 && r == 20 && bits == 128) {
      nbRounds = 2;
      inputDifference = new int[][]{{0,102,103,101},{0,0,0,101},{0,102,0,97},{0,102,0,101}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,0,202,0},{203,202,0,204},{202,0,204,0},{0,205,0,0}};
      lowerKeyDifference = new int[][][]{{{0,96,0,0},{0,0,0,0}}};
    }
    else if (tk == 3 && r == 22 && bits == 64) {
      nbRounds = 5;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,10,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,2},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,5}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,1,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,5},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,2}}};
    }
    else if (tk == 3 && r == 23 && bits == 64) {
      nbRounds = 5;
      inputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,10,0},{0,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,2},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,5}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,1}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,5,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{2,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 3 && r == 22 && bits == 128) {
      nbRounds = 3;
      inputDifference = new int[][]{{0,111,0,0},{0,0,0,0},{0,111,0,0},{0,111,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,96,0,0}},{{0,0,0,0},{0,0,0,0}}};
      outputDifference = new int[][]{{0,7,0,0},{222,0,0,0},{0,0,0,222},{0,0,222,0}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,141}},{{0,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 3 && r == 23 && bits == 128) {
      nbRounds = 4;
      inputDifference = new int[][]{{1,0,0,0},{0,0,0,0},{1,0,0,0},{1,0,0,0}};
      upperKeyDifference = new int[][][]{{{0,0,0,141},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,221}}};
      outputDifference = new int[][]{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,8}};
      lowerKeyDifference = new int[][][]{{{0,109,0,0},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}},{{56,0,0,0},{0,0,0,0}}};
    }
    else if (tk == 3 && r == 24 && bits == 128) {
      nbRounds = 4;
      inputDifference = new int[][]{{0,0,0,32},{0,0,0,0},{0,0,0,32},{0,0,0,32}};
      upperKeyDifference = new int[][][]{{{0,0,0,0},{0,0,131,0}},{{0,0,0,0},{0,0,0,0}},{{0,0,0,0},{0,198,0,0}}};
      outputDifference = new int[][]{{0,0,0,0},{0,1,0,193},{1,0,0,0},{0,0,0,1}};
      lowerKeyDifference = new int[][][]{{{0,0,0,0},{0,0,0,0}},{{0,0,0,227},{0,0,0,0}},{{0,0,0,0},{0,0,0,0}}};
    }
    System.out.println(new ProbaComputation(nbRounds, inputDifference, upperKeyDifference, outputDifference, lowerKeyDifference).getProbaTests(tk, r, bits));
  }

}
