package sandwichproba;


import com.github.phd.core.cryptography.attacks.boomerang.SPNSboxTables;
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;
import com.github.phd.core.cryptography.ciphers.skinny.Skinny4;
import com.github.phd.core.cryptography.ciphers.skinny.Skinny8;
import sandwichproba.sboxtransition.SPNFormula;
import sandwichproba.trails.*;
import sandwichproba.util.TreeVariable;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ProbaComputation {
    private final int nbRounds;
    private final int[][] inputDifference;
    private final int[][][] upperKeyDifference;
    private final int[][] outputDifference;
    private final int[][][] lowerKeyDifference;

    public ProbaComputation(final int nbRounds,
                            final int[][] inputDifference, final int[][][] upperKeyDifference,
                            final int[][] outputDifference, final int[][][] lowerKeyDifference) {
        this.nbRounds = nbRounds;
        this.inputDifference = inputDifference;
        this.upperKeyDifference = upperKeyDifference;
        this.outputDifference = outputDifference;
        this.lowerKeyDifference = lowerKeyDifference;
    }


    public Double getProbaTests(final int tk, final int r, final int bits) {
        System.out.println("tk" + tk + " " + r + "r " + bits + "bits");
        System.out.println("Generation of formula");

        final UpperTrail<Skinny> upperTrail = new SkinnyUpperTrail(nbRounds, inputDifference, upperKeyDifference);
        final LowerTrail<Skinny> lowerTrail = new SkinnyLowerTrail(nbRounds, outputDifference, lowerKeyDifference);

        final SPNFormula formula = new SPNFormulaGeneration(nbRounds, upperTrail, lowerTrail).getFormula();
        System.out.println("Formula:");
        System.out.println(formula);
        List<Variable> vars = formula.getVariables().stream().sorted().collect(Collectors.toList());
        System.out.println("Variables:");
        System.out.println(vars);
        SPNSboxTables sboxTables = new SPNSboxTables((bits == 64) ? Skinny4.Companion.getSBOX() : Skinny8.Companion.getSBOX(), 10);
        double result = 0.0;
        if (tk == 2 && r == 17 && bits == 64 ||
                tk == 1 && r == 14 && bits == 64) { // TK2 17r 64bits and tk1 14r 64bits
            TreeVariable tree = new TreeVariable(toSet(vars.get(0), vars.get(1), vars.get(11), vars.get(12), vars.get(13)),
                    Arrays.asList(new TreeVariable(toSet(vars.get(9)),//7 | 3 4
                                    Arrays.asList(new TreeVariable(toSet(vars.get(10), vars.get(6))),
                                            new TreeVariable(toSet(vars.get(4)),//10
                                                    Arrays.asList(new TreeVariable(toSet(vars.get(3))),//6
                                                            new TreeVariable(toSet(vars.get(8))))))),//9
                            new TreeVariable(toSet(vars.get(7)),
                                    Arrays.asList(new TreeVariable(toSet(vars.get(2))),
                                            new TreeVariable(toSet(vars.get(5))))),
                            new TreeVariable(toSet(vars.get(14), vars.get(15)))));
            tree.preetyPrint();
            result = formula.evaluateTree(sboxTables, tree, 0);
        } else if (tk == 0 && r == 11 && bits == 64 ||
                tk == 0 && r == 13 && bits == 128 ||
                tk == 1 && r == 15 && bits == 64 ||
                tk == 1 && r == 14 && bits == 128 ||
                tk == 1 && r == 15 && bits == 128 ||
                tk == 1 && r == 16 && bits == 128 ||
                tk == 1 && r == 17 && bits == 128 ||
                tk == 2 && r == 18 && bits == 64 ||
                tk == 2 && r == 18 && bits == 128 ||
                tk == 2 && r == 19 && bits == 64 ||
                tk == 2 && r == 19 && bits == 128 ||
                tk == 3 && r == 22 && bits == 64 ||
                tk == 3 && r == 23 && bits == 64 ||
                tk == 3 && r == 22 && bits == 128 ||
                tk == 3 && r == 23 && bits == 128 ||
                tk == 3 && r == 24 && bits == 128) {
            result = formula.evaluate(sboxTables);
        }
        return result;
    }

    public Double getProba(final int bits) {
        //System.out.println("Generation of formula");

        final UpperTrail<Skinny> upperTrail = new SkinnyUpperTrail(nbRounds, inputDifference, upperKeyDifference);
        final LowerTrail<Skinny> lowerTrail = new SkinnyLowerTrail(nbRounds, outputDifference, lowerKeyDifference);

        final SPNFormula formula = new SPNFormulaGeneration(nbRounds, upperTrail, lowerTrail).getFormula();
        System.out.println(formula);
        List<Variable> vars = formula.getVariables().stream().sorted().collect(Collectors.toList());
        System.out.println(vars);
        SPNSboxTables sboxTables = new SPNSboxTables((bits == 64) ? Skinny4.Companion.getSBOX() : Skinny8.Companion.getSBOX(), 10);
        double result = 0.0;
        result = formula.evaluate(sboxTables);
        return result;
    }

    private Set<Variable> toSet(Variable... vars) {
        return Arrays.stream(vars).collect(Collectors.toSet());
    }
}
