package sandwichproba.trails;

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr;
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;

public interface LowerTrail<Cipher> extends Trail<Cipher> {

    XorExpr getLinearExpressionAfterSbox(Variable v);

}
