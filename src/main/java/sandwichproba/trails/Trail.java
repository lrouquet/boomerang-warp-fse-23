package sandwichproba.trails;

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr;
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;

import java.util.Set;

public interface Trail<Cipher> {

    Variable getSboxState(int i, int j, int k);

    Set<Variable> getVariablesRequiredBy(Variable v);

    Set<Variable> getZeroVariables();

    XorExpr getSboxExpr(final int round, final int i, final int j);

}
