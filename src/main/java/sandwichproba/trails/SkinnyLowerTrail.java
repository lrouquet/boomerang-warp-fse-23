package sandwichproba.trails;

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr;
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents an upper trail of multiple rounds of a skinny cipher
 * 
 * @author Mathieu Vavrille
 */
public class SkinnyLowerTrail implements LowerTrail<Skinny> {
  private final int nbRounds;
  private final int[][] outputDifference;
  private final int[][][] keyDifferences;
  private Variable[][][] sboxStates; // The sboxStates are the input states of the sboxes
  private Map<Variable,Integer> affectation;

  /**
   * @param nbRounds the number of rounds on which to do the trail, not considering the linear part on the last round
   * @param outputDifference the output difference at the end of the trail, right after the sbox (not considering the last linear part)
   * @param keyDifferences the differences in the key for each round considered (except the last one)
   */
  public SkinnyLowerTrail(final int nbRounds, final int[][] outputDifference, final int[][][] keyDifferences) {
    this.nbRounds = nbRounds;
    this.outputDifference = outputDifference;
    this.keyDifferences = keyDifferences;
    affectation = new HashMap<Variable,Integer>();
    sboxStates = new Variable[nbRounds][4][4];
    for (int round = nbRounds-1; round >= 0; round--) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          final Variable currentVar = new Variable(round, i, j);
          sboxStates[round][i][j] = currentVar;
          if (getLinearExpressionAfterSbox(currentVar).isZero())
            affectation.put(currentVar, 0);
        }
      }
    }
  }

  /**
   * Propagates the linear part of the cipher
   * @param var a variable, ie an input of a sbox
   * @return The expression output of the sbox of the variable, considering the variables of the next round
   */
  @Override
  public XorExpr getLinearExpressionAfterSbox(final Variable var) {
    final int round = var.getPosRound();
    final int i = var.getPosI();
    final int j = var.getPosJ();
    if (round == nbRounds-1) {
      return XorExpr.ofConstant(outputDifference[i][j]);
    }
    XorExpr retExpr;
    switch (i) {
    case 0:
      retExpr = XorExpr.ofConstant(keyDifferences[round][0][j]).xor(sboxStates[round+1][1][j]);
      break;
    case 1:
      retExpr = XorExpr.ofConstant(keyDifferences[round][1][j]).xor(sboxStates[round+1][1][(j+1)%4]).xor(sboxStates[round+1][2][(j+1)%4]).xor(sboxStates[round+1][3][(j+1)%4]);
      break;
    case 2:
      retExpr = XorExpr.ofConstant(0).xor(sboxStates[round+1][1][(j+2)%4]).xor(sboxStates[round+1][3][(j+2)%4]);
      break;
    case 3:
      retExpr = XorExpr.ofConstant(0).xor(sboxStates[round+1][0][(j+3)%4]).xor(sboxStates[round+1][3][(j+3)%4]);
      break;
    default:
      throw new IllegalStateException("i should be 0, 1, 2 or 3");
    }
    return retExpr.evaluate(affectation);
  }

  /**
   * @param initialVariable
   * @return all the variables that can influence this variable
   */

  @Override
  public Set<Variable> getVariablesRequiredBy(final Variable initialVariable) {
    final Set<Variable> requiredVariables = new HashSet<Variable>();
    if (initialVariable.getPosRound() < nbRounds) {
      getLinearExpressionAfterSbox(initialVariable).getVariables().stream()
        .forEach(var -> {requiredVariables.addAll(getVariablesRequiredBy(var)); requiredVariables.add(var);});
    }
    return requiredVariables;
  }

  @Override
  public Variable getSboxState(final int round, final int i, final int j) {
    return sboxStates[round][i][j];
  }

  @Override
  public XorExpr getSboxExpr(final int round, final int i, final int j) {
    return XorExpr.ofVariable(getSboxState(round, i, j)).evaluate(affectation);
  }

  /**
   * @return all the variables equal to zero, given the output difference
   */
  @Override
  public Set<Variable> getZeroVariables() {
    return affectation.keySet();
  }
    
}
