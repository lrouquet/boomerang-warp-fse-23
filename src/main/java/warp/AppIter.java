package warp;//import org.chocosolver.solver.Solver;

import com.github.phd.core.cryptography.attacks.differential.Sbox;
import com.github.phd.core.mzn.*;
import com.github.phd.core.utils.ArgumentsKt;
import com.github.phd.core.utils.Logger;
import com.github.phd.core.utils.LoggerKt;
import com.github.phd.infra.MiniZincBinary;
import com.github.phd.infra.PicatBinary;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static warp.App.createFolderTree;

/*
    La recherche est supprimée des fichiers mzn et est ajoutée automatiquement par le code java ci-dessous.
    Ça permet d'avoir le même code pour la step1 opt et la step1 enum.

    Le dossier mzn_models contient trois fichiers:
    - WARP_Step1_Boomerang.mzn : le fichier contenant la step 1 à prorpement parler
    - WARP_Step1_Boomerang.optimized.mzn : le fichier contenant la step 1 à prorpement parler avec améliorations
    - WARP_Step1_Boomerang_decision_vars.mzn : le fichier contenant les variables pour l'heuristique de recherche
    - WARP_Step1_Boomerang_forbid_solution.mzn : le fichier donnant la contraint interdisant la solution précédente
 */
public class AppIter {

    private static final Config config = new Config("config.txt");

    private static final PartialMznModel MODEL =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang.optimized.mzn"));
    private static final PartialMznModel DECISION_VARS =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_decision_vars.mzn"));
    private static final PartialMznModel FORBID_SOLUTION_CONSTRAINT =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn"));

    /*
    Construit le modèle step 1 opt à partir du fichier MODEL
    La variable objective est "obj" et on cherche à minimizer.
    Le modèle complet est construit dans le fichier step1_opt.mzn
     */
    private static MznModel.Optimization STEP1_OPT() {
        return new MznModelBuilder.Optimization(
                Collections.singletonList(MODEL),
                new MznVariable("obj"),
                OptimizationSearch.Minimize,
                DECISION_VARS,
                null
        ).build(new File("mzn_models/tmp/step1_iter.mzn"));
    }

    /*
    Construit le modèle step 1 enum à partir du fichier MODEL
    Les variables de décisions sont décrites dans le fichier DECIVION_VARS,
    la contrainte interdisant la solution prédécente est décrite dans le fichier FORBID_SOLUTION_CONSTRAINT
     */
    private static MznModel.PartialSearch STEP1_ENUM(int Nr) {
        return new MznModelBuilder.PartialAssignment(
                Collections.singletonList(MODEL),
                MznSearchConfiguration.Companion.from(
                        DECISION_VARS,
                        SearchStrategy.DomOverWDeg,
                        ValueSelector.InDomainMin
                ),
                FORBID_SOLUTION_CONSTRAINT,
                App::getPartialAssignment,
                null
        ).build(new File(format("mzn_models/tmp/step1_enum_%d.mzn", Nr)));
    }

    public static void main(String[] _args_) throws IOException {
        createFolderTree();

        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addSubscriber(System.out, Logger.HeaderMode.NONE, false, null);
        BufferedWriter logWriter = new BufferedWriter(new FileWriter("logs/warp-step1-opt-iter.log"));
        LoggerKt.getLogger().addSubscriber(logWriter, Logger.HeaderMode.NONE, true, Logger.Level.ALL);

        MznSolver step1OptSolver;
        switch (args.getOrDefault("Step1Solver", null)) {
            case "Picat":
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1OptSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                LoggerKt.getLogger().log("Step1Solver = Picat");
                break;
            default:
                LoggerKt.getLogger().error("Argument Step1Solver is empty or invalid. Valid solvers are : Picat or Gurobi.");
                return;
        }

        int NrInit = Integer.parseInt(args.getOrDefault("Nr", "3"));
        List<Integer> prevUB = Arrays.stream(args.getOrDefault("PrevUB", "").split(";")).filter(it -> !it.equals("")).map(Integer::parseInt).collect(Collectors.toList());
        assert prevUB.size() + 3 == NrInit;
        for (int Nr = NrInit; Nr < 41; Nr++) {
            LoggerKt.getLogger().log("Nr = " + Nr);
            Instant start = Instant.now();
            Integer objStep1 = runStep1Opt(Nr, prevUB, step1OptSolver);
            if (objStep1 == null) {
                LoggerKt.getLogger().warn("No solution was found, time: %s", secondsElapsedSince(start));
                return;
            } else {
                LoggerKt.getLogger().log("ObjStep1: 2^{-%d}, time: %s", objStep1, secondsElapsedSince(start));
                prevUB.add(objStep1);
            }
        }
    }

    private static Integer runStep1Opt(int Nr, List<Integer> prevUB, MznSolver solver) throws IOException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("Eb", 0);
        parameters.put("Ed", Nr);
        parameters.put("Ef", 0);
        MznModel.Optimization model = STEP1_OPT();
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            for (int i = 0; i < prevUB.size(); i++) {
                int r = i + 3;
                bf.append(String.format("constraint sum(round in 0..%d, col in BR_HALF) (isTable(round, col) + isDDT2(round, col)) >= %d;\n", r, prevUB.get(i) / 2));
            }
            if (Nr >= 15) {
                bf.append("constraint(\n");
                for (int i = 0; i <= Nr - 5; i++) {
                    bf.append("   (actives_upper[" + i + "] = 1 /\\ actives_upper[" + (i + 1) + "] = 1  /\\ actives_upper[" + (i + 2) + "] = 0 /\\ actives_upper[" + (i + 3) + "] = 1 /\\ actives_upper[" + (i + 4) + "] = 1)");
                    if (i <= Nr - 6) {
                        bf.append(" \\/\n");
                    }
                }
                bf.append("\n);\n");
            }
        } catch (IOException ioe) {}
        MznSolution step1OptSol = solver.optimize(model, parameters);
        if (step1OptSol == null) return null;

        Step1Solution step1Solution = new ParseMznStep1Solution(step1OptSol).parseSolution(0, Nr, 0);
        try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step1/warp-%d.bin", Nr)))) {
            oss.writeObject(step1Solution);
        }
        String[] lines = step1OptSol.getContent().split("\n");
        for (String line : lines) {
            if (line.startsWith("obj = ")) {
                return Integer.parseInt(line.substring("obj = ".length(), line.lastIndexOf(';')));
            }
        }

        throw new RuntimeException("Cannot parse obj from the model");
    }

    private static class Seconds {
        private final Duration duration;

        public Seconds(Duration duration) {
            this.duration = duration;
        }

        @Override
        public String toString() {
            return String.format("%.2fs", duration.toMillis() / 1000.0);
        }

        public static final Seconds ZERO = new Seconds(Duration.ZERO);

        public Seconds plus(Seconds other) {
            return new Seconds(duration.plus(other.duration));
        }

    }

    private static Seconds secondsElapsedSince(Instant start) {
        return new Seconds(Duration.between(start, Instant.now()));
    }

}
