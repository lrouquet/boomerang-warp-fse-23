package warp;

public class MinPow2 {

    private static final int MAX_RANGE = 1024;
    private final long[] log2 = new long[MAX_RANGE];

    public void add(MinPow2 rhs) {
        for (int i = 0; i < 1024; i++) {
            this.log2[i] += rhs.log2[i];
        }
        simplify();
    }

    public void add(int pow2) {
        if (pow2 > 0 && log2[pow2] == 1) {
            log2[pow2] = 0;
            add(pow2 - 1);
        } else {
            log2[pow2] += 1;
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (log2[0] != 0) {
            str.append(log2[0]);
            str.append(" + ");
        }
        for (int i = 1; i < MAX_RANGE; i++) {
            if (log2[i] != 0) {
                str.append("2^{-");
                str.append(i);
                str.append("} + ");
            }
        }

        int len = str.length();
        if (len > 0) {
            str.setLength(len - 3);
        } else {
            str.append("-∞");
        }

        return str.toString();
    }

    private void simplify() {
        for (int i = MAX_RANGE - 1; i > 0; i--) {
            while (log2[i] >> 1 > 0) {
                log2[i] -= (log2[i] >> 1) << 1;
                log2[i - 1] += 1;
            }
        }
    }

}
