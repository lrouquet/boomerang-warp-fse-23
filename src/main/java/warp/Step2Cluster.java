package warp;

import com.github.phd.core.utils.LoggerKt;
import org.chocosolver.solver.Identity;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.IntVar;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.String.format;

public class Step2Cluster {

    // TOUT CHANGER EN DEUX D
    // shuffle à mettre ici
    // public static final int[] shuffle= {31,6,29,14,1,12,21,8,27,2,3,0,25,4,23,10,15,22,13,30,17,28,5,24,11,18,19,16,9,20,7,26};

    public static final int[] Pi_even = {31, 29, 1, 21, 27, 3, 25, 23, 15, 13, 17, 5, 11, 19, 9, 7};
    public static final int[] Pi_odd = {6, 14, 12, 8, 2, 0, 4, 10, 22, 30, 28, 24, 18, 16, 20, 26};

    public final Model model = new Model();
    private final IntVar FREE = model.intVar("FREE", -1);
    private final IntVar ZERO = model.intVar("ZERO", 0);
    private final Tuples XOR_TUPLES = XOR_TUPLES();

    public final IntVar[][] δXupper;
    public final IntVar[][] δSXupper;

    public final IntVar[][] δXlower;
    public final IntVar[][] δSXlower;

    public final IntVar[][] proba;
    public final IntVar p;
    public final IntVar q;
    public final IntVar r;
    public int START;
    public int END;

    private static final int BR = 32;
    private static final int BR_HALF = 16;

    final List<Function<FeistelSboxTables, Double>> EXACT_SBOX_COMPUTATION = new ArrayList<>();

    public Step2Cluster(
            int Nr,
            FeistelSboxTables SBOX_TABLES,
            int[][] DXupper, int[][] freeXupper, int[][] freeSBupper,
            int[][] DXlower, int[][] freeXlower, int[][] freeSBlower,
            Step2 step2
    ) {
        this(Nr, SBOX_TABLES, DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower, step2, null, null);
    }

    public Step2Cluster(
            int Nr,
            FeistelSboxTables SBOX_TABLES,
            int[][] DXupper, int[][] freeXupper, int[][] freeSBupper,
            int[][] DXlower, int[][] freeXlower, int[][] freeSBlower,
            Step2 step2,
            Integer overrideStart,
            Integer overrideEnd
    ) {

        δXupper = byteVars(Nr + 1, DXupper, freeXupper);
        δSXupper = byteVars2(Nr, DXupper, freeSBupper);

        δXlower = byteVars(Nr + 1, DXlower, freeXlower);
        δSXlower = byteVars2(Nr, DXlower, freeSBlower);

        proba = new IntVar[Nr][BR_HALF];
        List<IntVar> probaUpper = new ArrayList<>();
        List<IntVar> probaMid = new ArrayList<>();
        List<IntVar> probaLower = new ArrayList<>();

        START = Integer.MAX_VALUE;
        END = Integer.MIN_VALUE;

        int nbSb = 0;
        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < BR_HALF; j++) {
                IntVar Delta_i = δXupper[i][2 * j];
                IntVar delta = δSXupper[i][j];
                IntVar Nabla_o = δXlower[i][2 * j];
                IntVar alpha = δSXlower[i][j];

                if (BoomerangRules.isDDTupper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationDDTBounds);
                    model.table(new IntVar[]{Delta_i, delta, proba[i][j]}, SBOX_TABLES.relationDDT).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.ddtLog2(Delta_i.getValue(), delta.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isDDTlower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationDDTBounds);
                    model.table(new IntVar[]{Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationDDT).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.ddtLog2(Nabla_o.getValue(), alpha.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isDDT2upper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationDDT2Bounds);
                    model.table(new IntVar[]{Delta_i, delta, proba[i][j]}, SBOX_TABLES.relationDDT2).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> 2 * sbox.ddtLog2(Delta_i.getValue(), delta.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isDDT2lower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationDDT2Bounds);
                    model.table(new IntVar[]{Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationDDT2).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> 2 * sbox.ddtLog2(Nabla_o.getValue(), alpha.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isFBCT(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationFBCTBounds);
                    model.table(new IntVar[]{Delta_i, Nabla_o, proba[i][j]}, SBOX_TABLES.relationFBCT).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.fbctLog2(Delta_i.getValue(), Nabla_o.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isFBDTUpper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationFBDTBounds);
                    model.table(new IntVar[]{Delta_i, delta, Nabla_o, proba[i][j]}, SBOX_TABLES.relationFBDT).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.fbdtLog2(Delta_i.getValue(), delta.getValue(), Nabla_o.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isFBDTLower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationFBDTBounds);
                    model.table(new IntVar[]{Nabla_o, alpha, Delta_i, proba[i][j]}, SBOX_TABLES.relationFBDT).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.fbdtLog2(Nabla_o.getValue(), alpha.getValue(), Delta_i.getValue()));
                    nbSb += 1;
                } else if (BoomerangRules.isFBET(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    START = min(START, i);
                    END = max(END, i);
                    proba[i][j] = intVar(format("p[%d][%d]", i, j), SBOX_TABLES.relationFBETBounds);
                    model.table(new IntVar[]{Delta_i, delta, Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationFBET).post();
                    EXACT_SBOX_COMPUTATION.add(sbox -> sbox.fbetLog2(Delta_i.getValue(), delta.getValue(), Nabla_o.getValue(), alpha.getValue()));
                    nbSb += 1;
                }
            }
        }

        LoggerKt.getLogger().info("Number of active S-Boxes: %d", nbSb);

        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < BR_HALF; j++) {
                δXupper[i + 1][Pi_even[j]].eq(δXupper[i][2 * j]).post();
                δXlower[i + 1][Pi_even[j]].eq(δXlower[i][2 * j]).post();

                postXor(δXupper[i + 1][Pi_odd[j]], δXupper[i][2 * j + 1], δSXupper[i][j]);
                postXor(δXlower[i + 1][Pi_odd[j]], δXlower[i][2 * j + 1], δSXlower[i][j]);
            }
        }

        if (overrideStart != null) {
            START = overrideStart;
        }

        if (overrideEnd != null) {
            END = overrideEnd;
        }

        // 2D
        // List<IntVar> activeSboxes = new ArrayList<>();
        List<IntVar> midVars = new ArrayList<>();
        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < BR_HALF; j++) {
                if (proba[i][j] != null) {
                    // activeSboxes.add(proba[i][j]);
                    if (i < START) {
                        probaUpper.add(proba[i][j]);
                    } else if (i <= END) {
                        probaMid.add(proba[i][j]);
                        midVars.add(proba[i][j]);
                        midVars.add(δXupper[i][j]);
                        midVars.add(δSXupper[i][j]);
                        midVars.add(δXlower[i][j]);
                        midVars.add(δSXlower[i][j]);
                    } else {
                        probaLower.add(proba[i][j]);
                    }
                }
            }
        }

        for (int k = 0; k < BR; k++) {
            model.arithm(δXupper[START][k], "=", step2.δXupper[START][k]).post();
            model.arithm(δXlower[END + 1][k], "=", step2.δXlower[END + 1][k]).post();
        }

        p = model.intVar(0, 1024);
        model.sum(probaUpper.toArray(new IntVar[]{}), "=", p).post();
        q = model.intVar(0, 1024);
        model.sum(probaLower.toArray(new IntVar[]{}), "=", q).post();
        r = model.intVar(0, 1024);
        model.sum(probaMid.toArray(new IntVar[]{}), "=", r).post();

        midVars = midVars.stream()
                .filter(Objects::nonNull)
                .filter(distinctBy(Identity::getId))
                .collect(Collectors.toList());

        model.getSolver().setSearch(
                Search.intVarSearch(midVars.toArray(new IntVar[]{}))
        );
    }

    private static <T> Predicate<T> distinctBy(Function<? super T, ?> key) {
        Set<Object> seen = new HashSet<>();
        return it -> seen.add(key.apply(it));
    }

    private IntVar intVar(Bounds bounds) {
        return model.intVar(bounds.min, bounds.max);
    }

    private IntVar intVar(String name, Bounds bounds) {
        return model.intVar(name, bounds.min, bounds.max);
    }

    // Affectation de l'état => passage à 2 D
    // donc deltastate porte une troisième dimension contenant si il est libre ou non
    // en sortie en fait, on a une variable qui vaut diff(0), ZERO ou FREE
    // A MODIFIER PB DU 2*I pas de la même longueur
    // TABLEAU 32
    private IntVar[][] byteVars(int Nr, int[][] Δstate, int[][] freeState) {
        IntVar[][] δstate = new IntVar[Δstate.length][];
        for (int i = 0; i < Nr; i++) {
            δstate[i] = new IntVar[Δstate[i].length];
            for (int j = 0; j < Δstate[i].length; j++) {
                // δstate[i][j] = new IntVar[Δstate[i][j].length];
                // for (int k = 0; k < Δstate[i][j].length; k++) {
                if (freeState[i][j] == 0) {
                    if (Δstate[i][j] == 1) {
                        δstate[i][j] = model.intVar(1, 15);
                    } else {
                        δstate[i][j] = ZERO;
                    }
                } else {
                    δstate[i][j] = FREE;
                }
            }
        }

        return δstate;
    }

    // TABLEAU 16
    private IntVar[][] byteVars2(int Nr, int[][] Δstate, int[][] freeSBState) {
        IntVar[][] δstate = new IntVar[Δstate.length][];
        for (int i = 0; i < Nr; i++) {
            δstate[i] = new IntVar[Δstate[i].length / 2];
            for (int j = 0; j < Δstate[i].length / 2; j++) {
                // δstate[i][j] = new IntVar[Δstate[i][j].length];
                // for (int k = 0; k < Δstate[i][j].length; k++) {
                if (freeSBState[i][j] == 0) { // ATTENTION ICI C'est forcement FreeSB qui est appelé
                    if (Δstate[i][2 * j] == 1) {
                        δstate[i][j] = model.intVar(1, 15);
                    } else {
                        δstate[i][j] = ZERO;
                    }
                } else {
                    δstate[i][j] = FREE;
                }
            }
        }

        return δstate;
    }

    private void postXor(IntVar a, IntVar b, IntVar c) {
        if (a == FREE || b == FREE || c == FREE) return;

        if (a == ZERO && b == ZERO && c == ZERO) return;

        if (a == ZERO && b == ZERO) {
            model.arithm(c, "=", ZERO).post();
            return;
        }

        if (b == ZERO && c == ZERO) {
            model.arithm(a, "=", ZERO).post();
            return;
        }

        if (a == ZERO && c == ZERO) {
            model.arithm(b, "=", ZERO).post();
            return;
        }

        if (a == ZERO) {
            model.arithm(b, "=", c).post();
            return;
        }

        if (b == ZERO) {
            model.arithm(c, "=", a).post();
            return;
        }

        if (c == ZERO) {
            model.arithm(a, "=", b).post();
            return;
        }

        if (a.getLB() > 0) {
            model.arithm(b, "!=", c).post();
        }

        if (b.getLB() > 0) {
            model.arithm(a, "!=", c).post();
        }

        if (c.getLB() > 0) {
            model.arithm(b, "!=", a).post();
        }

        model.table(new IntVar[]{a, b, c}, XOR_TUPLES).post();
    }

    // changement taille
    private Tuples XOR_TUPLES() {
        Tuples tuples = new Tuples();

        for (int x = 0; x < BR_HALF; x++) {
            for (int y = 0; y < BR_HALF; y++) {
                tuples.add(x, y, x ^ y);
            }
        }

        return tuples;
    }

}
