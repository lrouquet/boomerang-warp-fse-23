package warp;

import org.chocosolver.solver.variables.IntVar;

import java.io.Serializable;

public class Step2 implements Serializable {

    public final int[][] δXupper;
    public final int[][] δSXupper;

    public final int[][] δXlower;
    public final int[][] δSXlower;

    public final int[][] proba;

    int objective;

    public Step2(Step2Model model) {
        δXupper = values(model.δXupper);
        δSXupper = values(model.δSXupper);
        δXlower = values(model.δXlower);
        δSXlower = values(model.δSXlower);
        proba = values(model.proba);
        objective = model.objective.getValue();
    }

    public Step2(Step2ModelWithStep3 model) {
        δXupper = values(model.δXupper);
        δSXupper = values(model.δSXupper);
        δXlower = values(model.δXlower);
        δSXlower = values(model.δSXlower);
        proba = values(model.proba);
        objective = model.objective.getValue();
    }

    private int[][] values(IntVar[][] vars) {
        int[][] values = new int[vars.length][];
        for (int i = 0; i < vars.length; i++) {
            if (vars[i] != null) {
                values[i] = new int[vars[i].length];
                for (int k = 0; k < vars[i].length; k++) {
                    if (vars[i][k] != null) {
                        values[i][k] = vars[i][k].getValue();
                    }
                }
            }
        }
        return values;
    }

}
