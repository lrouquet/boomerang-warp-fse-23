package warp;//import org.chocosolver.solver.Solver;

import com.github.phd.core.cryptography.attacks.differential.Sbox;
import com.github.phd.core.mzn.*;
import com.github.phd.core.utils.ArgumentsKt;
import com.github.phd.core.utils.Logger;
import com.github.phd.core.utils.LoggerKt;
import com.github.phd.infra.MiniZincBinary;
import com.github.phd.infra.PicatBinary;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import utils.SolutionCheckerKt;
import utils.SolutionWriterKt;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.lang.String.format;

/*
    La recherche est supprimée des fichiers mzn et est ajoutée automatiquement par le code java ci-dessous.
    Ça permet d'avoir le même code pour la step1 opt et la step1 enum.

    Le dossier mzn_models contient trois fichiers:
    - WARP_Step1_Boomerang.mzn : le fichier contenant la step 1 à prorpement parler
    - WARP_Step1_Boomerang.optimized.mzn : le fichier contenant la step 1 à prorpement parler avec améliorations
    - WARP_Step1_Boomerang_decision_vars.mzn : le fichier contenant les variables pour l'heuristique de recherche
    - WARP_Step1_Boomerang_forbid_solution.mzn : le fichier donnant la contraint interdisant la solution précédente
 */
public class App {

    private static final Config config = new Config("config.txt");

    public static final String OBJECTIVE = "obj";

    private static final PartialMznModel MODEL =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang.optimized.mzn"));
    private static final PartialMznModel DECISION_VARS =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_decision_vars.mzn"));
    private static final PartialMznModel FORBID_SOLUTION_CONSTRAINT =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn"));

    /*
    Construit le modèle step 1 opt à partir du fichier MODEL
    La variable objective est "obj" et on cherche à minimizer.
    Le modèle complet est construit dans le fichier step1_opt.mzn
     */
    private static MznModel.Optimization STEP1_OPT(int Nr) {
        MznModel.Optimization model = new MznModelBuilder.Optimization(
                Collections.singletonList(MODEL),
                new MznVariable(OBJECTIVE),
                OptimizationSearch.Minimize,
                DECISION_VARS,
                null
        ).build(new File(format("mzn_models/tmp/step1_opt_%d.mzn", Nr)));

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            if (Nr >= 15) {
                bf.append("constraint(\n");
                for (int i = 0; i <= Nr - 5; i++) {
                    bf.append("   (actives_upper[" + i + "] = 1 /\\ actives_upper[" + (i + 1) + "] = 1  /\\ actives_upper[" + (i + 2) + "] = 0 /\\ actives_upper[" + (i + 3) + "] = 1 /\\ actives_upper[" + (i + 4) + "] = 1)");
                    if (i <= Nr - 6) {
                        bf.append(" \\/\n");
                    }
                }
                bf.append("\n);\n");
            }
        } catch (IOException ioe) {}

        return model;
    }

    /*
    Construit le modèle step 1 enum à partir du fichier MODEL
    Les variables de décisions sont décrites dans le fichier DECIVION_VARS,
    la contrainte interdisant la solution prédécente est décrite dans le fichier FORBID_SOLUTION_CONSTRAINT
     */
    public static MznModel.PartialSearch STEP1_ENUM(int Nr) {

        MznModel.PartialSearch model = new MznModelBuilder.PartialAssignment(
                Collections.singletonList(MODEL),
                MznSearchConfiguration.Companion.from(
                        DECISION_VARS,
                        SearchStrategy.DomOverWDeg,
                        ValueSelector.InDomainMin
                ),
                FORBID_SOLUTION_CONSTRAINT,
                App::getPartialAssignment,
                null
        ).build(new File(format("mzn_models/tmp/step1_enum_%d.mzn", Nr)));

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            if (Nr >= 15) {
                bf.append("constraint(\n");
                for (int i = 0; i <= Nr - 5; i++) {
                    bf.append("   (actives_upper[" + i + "] = 1 /\\ actives_upper[" + (i + 1) + "] = 1  /\\ actives_upper[" + (i + 2) + "] = 0 /\\ actives_upper[" + (i + 3) + "] = 1 /\\ actives_upper[" + (i + 4) + "] = 1)");
                    if (i <= Nr - 6) {
                        bf.append(" \\/\n");
                    }
                }
                bf.append("\n);\n");
            }
        } catch (IOException ioe) {}

        return model;
    }

    public static final int[] WARP_SBOX = new int[]{
            0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6
    };

    public static final FeistelSboxTables SBOXES = new FeistelSboxTables(new Sbox(WARP_SBOX));

    // La fonction est utilisée pour supprimer les anciennes solutions dans la step 1.
    public static Assignment.Partial getPartialAssignment(MznSolution warpMznSolution) {
        StringBuilder builder = new StringBuilder();

        String[] subSolutions = warpMznSolution.getContent().split("----------");
        String[] lines = subSolutions[subSolutions.length - 2].split("\n");

        boolean[] DXupper = getBoolArray(lines, "DXupper");
        builder.append("array [ROUNDS, BR] of var bool: DXupper_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(DXupper))
                .append(");\n");

        boolean[] FreeXupper = getBoolArray(lines, "FreeXupper");
        builder.append("array [ROUNDS, BR] of var bool: FreeXupper_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(FreeXupper))
                .append(");\n");

        boolean[] FreeSBupper = getBoolArray(lines, "FreeSBupper");
        builder.append("array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBupper_%SOL% = array2d(ATTACK_ROUNDS, BR_HALF, ")
                .append(Arrays.toString(FreeSBupper))
                .append(");\n");

        boolean[] DXlower = getBoolArray(lines, "DXlower");
        builder.append("array [ROUNDS, BR] of var bool: DXlower_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(DXlower))
                .append(");\n");

        boolean[] FreeXlower = getBoolArray(lines, "FreeXlower");
        builder.append("array [ROUNDS, BR] of var bool: FreeXlower_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(FreeXlower))
                .append(");\n");

        boolean[] FreeSBlower = getBoolArray(lines, "FreeSBlower");
        builder.append("array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBlower_%SOL% = array2d(ATTACK_ROUNDS, BR_HALF, ")
                .append(Arrays.toString(FreeSBlower))
                .append(");\n");

        return new Assignment.Partial(builder.toString());
    }

    public static void main(String[] _args_) throws IOException {
        createFolderTree();
        Seconds step1OptTime = Seconds.ZERO;
        Seconds step1EnumTime = Seconds.ZERO;
        Seconds step2OptTime = Seconds.ZERO;

        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");
        int Nr = Integer.parseInt(ArgumentsKt.expectArgument(args, "Nr"));

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addSubscriber(System.out, Logger.HeaderMode.NONE, false, null);
        String logFileName = format("logs/warp-%d.log", Nr);
        BufferedWriter logWriter = new BufferedWriter(new FileWriter(logFileName));
        LoggerKt.getLogger().addSubscriber(logWriter, Logger.HeaderMode.NONE, true, Logger.Level.ALL);

        MznSolver step1OptSolver;
        switch (args.get("SolverStep1Opt")) {
            case "Picat":
                LoggerKt.getLogger().info("Selected solver for Step1 Opt = Picat");
                // Indique à Java où est l'exécutable de Minizinc pour convertir les mzn en fzn
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1OptSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                break;
            default:
                LoggerKt.getLogger().error("You must select a Step1 Opt Solver between Picat and Gurobi");
                return;
        }

        MznSolver step1EnumSolver;
        switch (args.get("SolverStep1Enum")) {
            case "Picat":
                LoggerKt.getLogger().info("Selected solver for Step1 Enum = Picat");
                // Indique à Java où est l'exécutable de Minizinc pour convertir les mzn en fzn
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1EnumSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                break;
            default:
                LoggerKt.getLogger().info("No Solver Step1 Enum is given. Using Solver Step1 Opt as Solver Step1 Enum.");
                return;
        }

        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("Eb", 0);
        parameters.put("Ed", Nr);
        parameters.put("Ef", 0);

        int obj;
        if (!args.containsKey("ObjStep1")) {
            LoggerKt.getLogger().info("No ObjStep1 is given, starting Step1Opt for Nr = " + Nr);
            Instant start = Instant.now();
            Integer objStep1 = runStep1Opt(Nr, parameters, step1OptSolver);
            LoggerKt.getLogger().log("Time to solve Step1Opt = %s", secondsElapsedSince(start));
            step1OptTime = step1OptTime.plus(secondsElapsedSince(start));
            if (objStep1 == null) {
                LoggerKt.getLogger().warn("No solution was found");
                return;
            } else {
                obj = objStep1;
            }

        } else {
            obj = Integer.parseInt(args.get("ObjStep1"));
            if (obj < 0) {
                obj = -obj;
            }
        }
        LoggerKt.getLogger().info("ObjStep1 = " + obj);

        int ub = obj;
        parameters.put(OBJECTIVE, ub);
        int lb = Integer.MAX_VALUE;

        MznModel.PartialSearch step1Enum = STEP1_ENUM(Nr);
        Iterator<MznSolution> iterator = step1EnumSolver.enumerate(step1Enum, parameters);
        int counter = 0;

        int LIMIT = -1;
        if (args.containsKey("Limit")) {
            LIMIT = Integer.parseInt(args.get("Limit"));
            LoggerKt.getLogger().info("User Limit = " + LIMIT);
        }

        while (-lb < -ub) {
            Instant step1Start = Instant.now();
            LoggerKt.getLogger().log("Searching new Step1 solution for with UB = 2^{-%d}, LB = 2^{-%d}", ub, lb);
            if ((LIMIT < 0 || (counter < LIMIT)) && iterator.hasNext()) {
                MznSolution solution = iterator.next();
                LoggerKt.getLogger().log("Step 1 solution found (%s)", secondsElapsedSince(step1Start));
                step1EnumTime = step1EnumTime.plus(secondsElapsedSince(step1Start));

                Step1Solution step1Solution = new ParseMznStep1Solution(solution).parseSolution(0, Nr, 0);

                Step2Model step2 = new Step2Model(
                        Nr, SBOXES, lb,
                        step1Solution.ΔXupper, step1Solution.freeXupper, step1Solution.freeSXupper,
                        step1Solution.ΔXlower, step1Solution.freeXlower, step1Solution.freeSXlower
                );

                Solver s = step2.model.getSolver();
                Instant step2Start = Instant.now();
                LoggerKt.getLogger().trace("Searching new best warp.Step2 solution");
                while (s.solve()) {
                    lb = step2.objective.getValue();
                    LoggerKt.getLogger().log("Step 2 solution found. Probability = 2^{-%d} (%s)", step2.objective.getValue(), secondsElapsedSince(step2Start));
                    LoggerKt.getLogger().trace("Writing solution to solutions/step2/warp-%d.tex", Nr);
                    SolutionWriterKt.buildLatex(Nr, step2).writeTo(format("solutions/step2/warp-%d.tex", Nr));
                    LoggerKt.getLogger().trace("Writing solution to solutions/step1/warp-%d.bin", Nr);
                    try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step1/warp-%d.bin", Nr)))) {
                        oss.writeObject(step1Solution);
                    }
                    LoggerKt.getLogger().trace("Writing solution to solutions/step2/warp-%d.bin", Nr);
                    try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step2/warp-%d.bin", Nr)))) {
                        oss.writeObject(new Step2(step2));
                    }

                    int[] gamma = Arrays.stream(step2.δXupper[0]).mapToInt(IntVar::getValue).toArray();
                    int[] delta = Arrays.stream(step2.δXlower[Nr]).mapToInt(IntVar::getValue).toArray();
                    SolutionCheckerKt.writeTo(Nr, gamma, delta, format("solutions/verif/warp-%d.rs", Nr));

                    StringBuilder str = new StringBuilder();
                    for (int i = 0; i < Nr; i++) {
                        str.setLength(0);
                        for (int j = 0; j < 16; j++) {
                            if (step1Solution.tables[i][j] != null) {
                                str.append(format("%6s| ", step1Solution.tables[i][j]));
                            } else {
                                str.append("      | ");
                            }
                        }
                        LoggerKt.getLogger().info(str.toString());
                        LoggerKt.getLogger().info("------+-".repeat(16));
                    }

                    double exactProba = step2.EXACT_SBOX_COMPUTATION.stream().mapToDouble(it -> it.apply(SBOXES)).sum();
                    LoggerKt.getLogger().log("Exact proba: 2^{%.2f}", exactProba);
                    step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
                    step2Start = Instant.now();
                }
                LoggerKt.getLogger().log("No more Step 2 solution (%s)", secondsElapsedSince(step2Start));
                step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
                counter += 1;
            } else {
                if ((LIMIT > 0 && counter == LIMIT)) {
                    LoggerKt.getLogger().log("Step 1 iteration limit reached for ub = %d. Upgrade ub = %d", ub, (ub + 1));
                } else {
                    LoggerKt.getLogger().log("No Step 1 solution was found. Upgrade ub = " + (ub + 1));
                }
                step1EnumTime = step1EnumTime.plus(secondsElapsedSince(step1Start));
                LoggerKt.getLogger().log("Time to end Step 1 search for ub = %d, %s", ub, secondsElapsedSince(step1Start));
                counter = 0;
                ub += 1;
                parameters.put(OBJECTIVE, ub);
                iterator = step1EnumSolver.enumerate(step1Enum, parameters);
            }
        }

        LoggerKt.getLogger().log("Step1OptTime\tStep1EnumTime\tStep2OptTime\tBest proba");
        LoggerKt.getLogger().log("%s\t%s\t%s\t2^{-%d}", step1OptTime, step1EnumTime, step2OptTime, lb);

    }

    private static boolean[] getBoolArray(String[] lines, String key) {
        for (String line : lines) {
            if (line.startsWith(key)) {
                return parseBooleanArray(line);
            }
        }
        return null;
    }

    private static boolean[] parseBooleanArray(String line) {
        String[] dims = line.substring(line.indexOf('(') + 1, line.indexOf("[") - 1).split(",");
        int array_len = 1;
        for (String dim : dims) {
            String[] range = dim.split("\\.\\.");
            int start = Integer.parseInt(range[0]);
            int end = Integer.parseInt(range[1]);
            int length = end - start + 1;
            array_len *= length;
        }
        String[] values = line.substring(line.indexOf('[') + 1, line.lastIndexOf(']'))
                .split(",");
        assert values.length == array_len;
        boolean[] result = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            assert Arrays.asList("true", "false", "0", "1").contains(values[i]);
            result[i] = (values[i].equalsIgnoreCase("true") || values[i].equalsIgnoreCase("1"));
        }
        return result;
    }

    public static void createFolderTree() {
        new File("logs").mkdirs();
        new File("mzn_models/tmp").mkdirs();
        new File("solutions/step1").mkdirs();
        new File("solutions/step1-cluster").mkdirs();
        new File("solutions/step1-time-complexity").mkdirs();
        new File("solutions/step2").mkdirs();
        new File("solutions/step2-cluster").mkdirs();
        new File("solutions/step2-time-complexity").mkdirs();
        new File("solutions/verif").mkdirs();
        new File("solutions/verif-time-complexity").mkdirs();
    }

    public static Integer runStep1Opt(int Nr, HashMap<String, Object> parameters, MznSolver solver) throws IOException {
        MznSolution step1OptSol = solver.optimize(STEP1_OPT(Nr), parameters);
        if (step1OptSol == null) return null;

        String[] lines = step1OptSol.getContent().split("\n");
        for (String line : lines) {
            if (line.startsWith(OBJECTIVE + " = ")) {
                return Integer.parseInt(line.substring((OBJECTIVE + " = ").length(), line.lastIndexOf(';')));
            }
        }

        throw new RuntimeException(format("Cannot parse %s from the model", OBJECTIVE));
    }

    public static class Seconds {
        private final Duration duration;

        public Seconds(Duration duration) {
            this.duration = duration;
        }

        @Override
        public String toString() {
            return format("%.2fs", duration.toMillis() / 1000.0);
        }

        public static final Seconds ZERO = new Seconds(Duration.ZERO);

        public Seconds plus(Seconds other) {
            return new Seconds(duration.plus(other.duration));
        }

    }

    public static Seconds secondsElapsedSince(Instant start) {
        return new Seconds(Duration.between(start, Instant.now()));
    }

}
