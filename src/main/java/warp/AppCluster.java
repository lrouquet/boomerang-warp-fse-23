package warp;

import com.github.phd.core.cryptography.attacks.differential.Sbox;
import com.github.phd.core.files.latex.Latex;
import com.github.phd.core.mzn.*;
import com.github.phd.core.utils.ArgumentsKt;
import com.github.phd.core.utils.Logger;
import com.github.phd.core.utils.LoggerKt;
import com.github.phd.infra.MiniZincBinary;
import com.github.phd.infra.PicatBinary;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;
import sboxtransition.FeistelSboxTransition;
import utils.SolutionWriterKt;

import java.io.*;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static warp.App.createFolderTree;
import static warp.App.secondsElapsedSince;

public class AppCluster {

    private static final Config config = new Config("config.txt");

    private static final PartialMznModel MODEL =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang.optimized.mzn"));
    private static final PartialMznModel CLUSTER_EXTENSION =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_cluster_extension.mzn"));
    private static final PartialMznModel FORBID_SOLUTION_CONSTRAINT =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_cluster_forbid_solution.mzn"));

    public static final int[] WARP_SBOX = new int[]{
            0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6
    };

    public static final FeistelSboxTables SBOXES = new FeistelSboxTables(new Sbox(WARP_SBOX));

    public static void main(String[] _args_) throws IOException, ClassNotFoundException {
        createFolderTree();
        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");

        String mode = args.get("Mode");

        int Eb;
        int Ed;
        int Ef;
        int Nr;
        Step1Solution step1Solution;
        Step2 step2;

        if (mode.equals("Attack")) {
            Eb = parseInt(args.get("Eb"));
            Ed = parseInt(args.get("Ed"));
            Ef = parseInt(args.get("Ef"));
            Nr = Eb + Ed + Ef;
            String step1Folder = args.getOrDefault("Step1Folder", "solutions/step1-time-complexity");
            String step2Folder = args.getOrDefault("Step2Folder", "solutions/step2-time-complexity");

            step1Solution = (Step1Solution) new ObjectInputStream(new FileInputStream(format("%s/warp-%d-%d-%d.bin", step1Folder, Eb, Ed, Ef))).readObject();
            step2 = (Step2) new ObjectInputStream(new FileInputStream(format("%s/warp-%d-%d-%d.bin", step2Folder, Eb, Ed, Ef))).readObject();
        } else if (mode.equals("Distinguisher")) {
            Eb = 0;
            Ef = 0;
            Nr = parseInt(args.get("Nr"));
            Ed = Nr;
            String step1Folder = args.getOrDefault("Step1Folder", "solutions/step1");
            String step2Folder = args.getOrDefault("Step2Folder", "solutions/step2");

            step1Solution = (Step1Solution) new ObjectInputStream(new FileInputStream(format("%s/warp-%d.bin", step1Folder, Nr))).readObject();
            step2 = (Step2) new ObjectInputStream(new FileInputStream(format("%s/warp-%d.bin", step2Folder, Nr))).readObject();
        } else {
            throw new IllegalArgumentException(format("Invalide mode %s, expected are: {Attack, Distinguisher}", mode));
        }

        String mod = args.get("Selection");

        int probaOpt = parseInt(args.get("ProbaOpt"));
        int probaLimit = parseInt(args.get("ProbaLimit"));

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addTerminal();

        int START;
        int END;

        if ("AUTO".equals(mod)) {
            Step2Cluster cluster = new Step2Cluster(
                    Nr,
                    SBOXES,
                    step1Solution.ΔXupper, step1Solution.freeXupper, step1Solution.freeSXupper,
                    step1Solution.ΔXlower, step1Solution.freeXlower, step1Solution.freeSXlower,
                    step2
            );
            START = cluster.START;
            END = cluster.END;
            LoggerKt.getLogger().info("AUTO Mod, start = %d, end = %d", START, END);
        } else if ("MANUAL".equals(mod)) {
            START = parseInt(args.get("Start"));
            END = parseInt(args.get("End"));
            LoggerKt.getLogger().info("Manual Mod, start = %d, end = %d", START, END);
        } else {
            LoggerKt.getLogger().fatal("Invalid option mod. Accepted are {AUTO, MANUAL}, given is %s", mod);
            return;
        }

        MiniZincBinary minizinc =
                new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

        PicatBinary picat = new PicatBinary(
                args.getOrDefault("Picat", config.get("Picat")),
                args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                minizinc
        );

        HashMap<String, Object> data = new HashMap<>();
        data.put("Eb", Eb);
        data.put("Ed", Ed);
        data.put("Ef", Ef);

        data.put("START", START);
        data.put("END", END);

        Iterator<MznSolution> solutionIterator = picat.enumerate(STEP1_ENUM(Nr, START, END, probaOpt, probaLimit, step1Solution), data);

        MinPow2 countProba = new MinPow2();
        while (solutionIterator.hasNext()) {
            step1Solution = new ParseMznStep1Solution(solutionIterator.next()).parseSolution(0, Nr, 0);
            LoggerKt.getLogger().log("New Step1 sol");

            Step2Cluster step2Cluster = new Step2Cluster(
                    Nr,
                    SBOXES,
                    step1Solution.ΔXupper, step1Solution.freeXupper, step1Solution.freeSXupper,
                    step1Solution.ΔXlower, step1Solution.freeXlower, step1Solution.freeSXlower,
                    step2, START, END
            );

            Instant step2Start = Instant.now();

            int nbSolutions = 0;
            int log = 1;
            int logNbSolutions = -1;

            StringBuilder str = new StringBuilder();
            for (int i = 0; i < Nr; i++) {
                str.setLength(0);
                for (int j = 0; j < 16; j++) {
                    if (step1Solution.tables[i][j] != null) {
                        str.append(format("%6s| ", step1Solution.tables[i][j]));
                    } else {
                        str.append("      | ");
                    }
                }
                LoggerKt.getLogger().info(str.toString());
                LoggerKt.getLogger().info("------+-".repeat(16));
            }

            IntVar[] probaList = Arrays.stream(ArrayUtils.flatten(step2Cluster.proba))
                    .filter(Objects::nonNull)
                    .toArray(IntVar[]::new);
            step2Cluster.model.sum(probaList, "<=", probaLimit).post();
            step2Cluster.model.getSolver().limitSolution(1L << 20);

            while (step2Cluster.model.getSolver().solve()) {

                for (int k = 0; k < 32; k++) {
                    assert step2Cluster.δXupper[step2Cluster.START][k].getValue() == step2.δXupper[step2Cluster.START][k];
                    assert step2Cluster.δXlower[step2Cluster.END + 1][k].getValue() == step2.δXlower[step2Cluster.END + 1][k];
                }

                nbSolutions += 1;

                if (nbSolutions == 1) {
                    // LoggerKt.getLogger().log("p = 2^{-%d}", step2Cluster.p.getValue());
                    // LoggerKt.getLogger().log("q = 2^{-%d}", step2Cluster.q.getValue());
                    LoggerKt.getLogger().log("r = 2^{-%d}", step2Cluster.r.getValue());
                }

                countProba.add(step2Cluster.r.getValue());

                if (nbSolutions == log) {
                    log <<= 1;
                    logNbSolutions += 1;

                    LoggerKt.getLogger().log("2^{%d} %s", logNbSolutions, secondsElapsedSince(step2Start));
                    LoggerKt.getLogger().log("%s", countProba);
                    LoggerKt.getLogger().log(step2Cluster.model.getSolver().getMeasures().toOneLineString());
                }
            }

            LoggerKt.getLogger().log("%s, count = %d", countProba, nbSolutions);
        }

    }

    private static MznModel.PartialSearch STEP1_ENUM(int Nr, int START, int END, int PROBA_OPT, int PROBA_LIMIT, Step1Solution step1Solution) {
        MznModel.PartialSearch model = new MznModelBuilder.PartialAssignment(
                Arrays.asList(
                        MODEL,
                        CLUSTER_EXTENSION
                ),
                () -> new MznContent("solve minimize cluster_obj;"),
                FORBID_SOLUTION_CONSTRAINT,
                App::getPartialAssignment,
                null
        ).build(new File(format("mzn_models/tmp/step1_enum_cluster_%d.mzn", Nr)));

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            bf.append(format("constraint (obj >= %d);\n", PROBA_OPT));
            bf.append(format("constraint (obj <= %d);\n", PROBA_LIMIT));
            for (int k = 0; k < 32; k++) {
                bf.append(format("constraint DXupper[%d, %d] = %d;\n", START, k, step1Solution.ΔXupper[START][k]));
                bf.append(format("constraint FreeXupper[%d, %d] = %d;\n", START, k, step1Solution.freeXupper[START][k]));
            }
            for (int k = 0; k < 32; k++) {
                bf.append(format("constraint DXlower[%d, %d] = %d;\n", END + 1, k, step1Solution.ΔXlower[END + 1][k]));
                bf.append(format("constraint FreeXlower[%d, %d] = %d;\n", END + 1, k, step1Solution.freeXlower[END + 1][k]));
            }
        } catch (IOException ioe) {
        }

        return model;
    }

}
