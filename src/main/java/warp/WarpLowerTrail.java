package warp;

import com.github.phd.core.cryptography.attacks.boomerang.util.XorExpr;
import com.github.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;
import sandwichproba.trails.LowerTrail;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class WarpLowerTrail implements LowerTrail<Warp> {

    private static final int[] PI = new int[]{31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10, 15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26};

    private final int nbRounds;
    private final Variable[][] X;
    private final Variable[][] SX;
    private final Map<Variable, Integer> affectation;
    private final int[] PI_INV;

    public static void main(String[] test) {
        new WarpLowerTrail(1, new int[]{});
    }

    public WarpLowerTrail(final int nbRounds, final int[] inputDifference) {
        this.nbRounds = nbRounds;
        PI_INV = new int[32];
        for (int i = 0; i < 32; i++) {
            PI_INV[PI[i]] = i;
        }

        affectation = new HashMap<>();
        SX = new Variable[nbRounds][16];
        X = new Variable[nbRounds][32];

        for (int k = 0; k < 32; k++) {
            final Variable currentVar = new Variable(nbRounds - 1, 0, k);
            X[nbRounds - 1][k] = currentVar;
            affectation.put(currentVar, inputDifference[k]);
        }

        for (int k = 0; k < 16; k++) {
            final Variable sbVar = new Variable(nbRounds - 1, 0, k);
            SX[nbRounds - 1][k] = sbVar;
            if (getLinearExpressionAfterSbox(sbVar).isZero()) {
                affectation.put(sbVar, 0);
            }
        }

        for (int r = nbRounds - 2; r >= 0; r--) {
            for (int k = 0; k < 16; k++) {
                final Variable sbVar = new Variable(r, 0, k);
                SX[r][k] = sbVar;
                if (getLinearExpressionAfterSbox(sbVar).isZero()) {
                    affectation.put(sbVar, 0);
                }
                X[r][2 * k] = X[r + 1][PI[2 * k + 1]];
                X[r][2 * k + 1] = new Variable(r, 0, 2 * k + 1);
                if (getLinearExpressionAfterSbox(X[r][PI[2 * k]]).isZero()) {
                    affectation.put(X[r][PI[2 * k]], 0);
                }
            }
        }

    }

    @Override
    public Variable getSboxState(int i, int j, int k) {
        return SX[i][k];
    }

    @Override
    public Set<Variable> getVariablesRequiredBy(Variable v) {
        final Set<Variable> requiredVariables = new HashSet<>();
        if (v.getPosRound() > 0) {
            getLinearExpressionAfterSbox(v).getVariables()
                    .forEach(var -> {
                        requiredVariables.addAll(getVariablesRequiredBy(var));
                        requiredVariables.add(var);
                    });
        }
        return requiredVariables;
    }

    @Override
    public Set<Variable> getZeroVariables() {
        return affectation.keySet();
    }

    @Override
    public XorExpr getSboxExpr(int round, int i, int j) {
        assert i == 0;
        return XorExpr.ofVariable(getSboxState(round, i, j)).evaluate(affectation);
    }

    @Override
    public XorExpr getLinearExpressionAfterSbox(Variable v) {
        assert v.getPosI() == 0;
        final int r = v.getPosRound();
        final int k = v.getPosJ();

        if (r == this.nbRounds - 1) {
            return XorExpr.ofVariable(X[r][k]);
        } else {
            if (k % 2 == 0) {
                return XorExpr.ofVariable(X[r + 1][PI[k + 1]]);
            } else {
                return XorExpr.ofVariable(SX[r][PI[k / 2]]).xor(X[r + 1][PI[k]]);
            }
        }
    }
}
