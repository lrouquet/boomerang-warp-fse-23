package warp;

import com.github.phd.core.mzn.*;
import com.github.phd.core.utils.ArgumentsKt;
import com.github.phd.core.utils.Logger;
import com.github.phd.core.utils.Logger.Level;
import com.github.phd.core.utils.LoggerKt;
import com.github.phd.infra.MiniZincBinary;
import com.github.phd.infra.PicatBinary;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import utils.SolutionCheckerKt;
import utils.SolutionWriterKt;
import warp.App.Seconds;

import java.io.*;
import java.time.Instant;
import java.util.*;

import static java.lang.String.format;
import static warp.App.createFolderTree;
import static warp.App.secondsElapsedSince;

/*
    La recherche est supprimée des fichiers mzn et est ajoutée automatiquement par le code java ci-dessous.
    Ça permet d'avoir le même code pour la step1 opt et la step1 enum.

    Le dossier mzn_models contient trois fichiers:
    - WARP_Step1_Boomerang.mzn : le fichier contenant la step 1 à prorpement parler
    - WARP_Step1_Boomerang.optimized.mzn : le fichier contenant la step 1 à prorpement parler avec améliorations
    - WARP_Step1_Boomerang_decision_vars.mzn : le fichier contenant les variables pour l'heuristique de recherche
    - WARP_Step1_Boomerang_forbid_solution.mzn : le fichier donnant la contraint interdisant la solution précédente
 */
public class AppWithStep3 {

    private static final Config config = new Config("config.txt");

    private static final String OBJECTIVE = "time_complexity";

    private static final PartialMznModel MODEL =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang.optimized.mzn"));
    private static final PartialMznModel BEGIN_ATTACK_EXTENSION =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_begin_attack_extension.mzn"));
    private static final PartialMznModel END_ATTACK_EXTENSION =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_end_attack_extension.mzn"));
    private static final PartialMznModel DECISION_VARS =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_decision_vars.mzn"));
    private static final PartialMznModel FORBID_SOLUTION_CONSTRAINT =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn"));

    /*
    Construit le modèle step 1 opt à partir du fichier MODEL
    La variable objective est "obj" et on cherche à minimizer.
    Le modèle complet est construit dans le fichier step1_opt.mzn
     */
    private static MznModel.Optimization STEP1_OPT(int Eb, int Ed, int Ef) {
        List<PartialMznModel> modelParts = new ArrayList<>();
        modelParts.add(MODEL);

        StringBuilder output = new StringBuilder().append('"');
        output.append(show("obj"))
                .append(show("DXupper"))
                .append(show("FreeXupper"))
                .append(show("FreeSBupper"))
                .append(show("DXlower"))
                .append(show("FreeXlower"))
                .append(show("FreeSBlower"));

        if (Eb > 0) {
            modelParts.add(BEGIN_ATTACK_EXTENSION);
            output.append(show("known"));
            output.append(show("attack_III_stage_1"));
            output.append(show("attack_III_stage_2"));
            output.append(show("time_complexity"));
            output.append(show("mb"));
            output.append(show("rb"));
            output.append(show("rf"));
        }
        if (Ef > 0) {
            modelParts.add(END_ATTACK_EXTENSION);
        }
        output.append('"');

        MznModel.Optimization model = new MznModelBuilder.Optimization(
                modelParts,
                new MznVariable(OBJECTIVE),
                OptimizationSearch.Minimize,
                DECISION_VARS,
                () -> new MznContent(output.toString())
        ).build(new File(format("mzn_models/tmp/step1-opt-%d-%d-%d.mzn", Eb, Ed, Ef)));

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            if (Ed >= 15) {
                bf.append("constraint(\n");
                for (int i = 0; i <= Ed - 5; i++) {
                    int start = i + Eb;
                    bf.append("   (actives_upper[" + start + "] = 1 /\\ actives_upper[" + (start + 1) + "] = 1  /\\ actives_upper[" + (start + 2) + "] = 0 /\\ actives_upper[" + (start + 3) + "] = 1 /\\ actives_upper[" + (start + 4) + "] = 1)");
                    if (i <= Ed - 6) {
                        bf.append(" \\/\n");
                    }
                }
                bf.append("\n);\n");
            }
        } catch (IOException ioe) {
        }

        return model;
    }

    private static String show(String name) {
        return format("%s = \\(%s);\\n", name, name);
    }

    /*
    Construit le modèle step 1 enum à partir du fichier MODEL
    Les variables de décisions sont décrites dans le fichier DECIVION_VARS,
    la contrainte interdisant la solution prédécente est décrite dans le fichier FORBID_SOLUTION_CONSTRAINT
     */
    public static MznModel.PartialSearch STEP1_ENUM(int Eb, int Ed, int Ef) {
        List<PartialMznModel> modelParts = new ArrayList<>();
        modelParts.add(MODEL);

        StringBuilder output = new StringBuilder().append('"');
        output.append(show("obj"))
                .append(show("DXupper"))
                .append(show("FreeXupper"))
                .append(show("FreeSBupper"))
                .append(show("DXlower"))
                .append(show("FreeXlower"))
                .append(show("FreeSBlower"));

        if (Eb > 0) {
            modelParts.add(BEGIN_ATTACK_EXTENSION);
            output.append(show("known"));
            output.append(show("attack_III_stage_1"));
            output.append(show("attack_III_stage_2"));
            output.append(show("time_complexity"));
            output.append(show("mb"));
            output.append(show("rb"));
            output.append(show("rf"));
        }
        if (Ef > 0) {
            modelParts.add(END_ATTACK_EXTENSION);
        }
        output.append('"');

        MznModel.PartialSearch model = new MznModelBuilder.PartialAssignment(
                modelParts,
                () -> new MznContent("solve satisfy;"),
                FORBID_SOLUTION_CONSTRAINT,
                App::getPartialAssignment,
                () -> new MznContent(output.toString())
        ).build(new File(format("mzn_models/tmp/step1-enum-%d-%d-%d.mzn", Eb, Ed, Ef)));

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            if (Ed >= 15) {
                bf.append("constraint(\n");
                for (int i = 0; i <= Ed - 5; i++) {
                    int start = i + Eb;
                    bf.append("   (actives_upper[" + start + "] = 1 /\\ actives_upper[" + (start + 1) + "] = 1  /\\ actives_upper[" + (start + 2) + "] = 0 /\\ actives_upper[" + (start + 3) + "] = 1 /\\ actives_upper[" + (start + 4) + "] = 1)");
                    if (i <= Ed - 6) {
                        bf.append(" \\/\n");
                    }
                }
                bf.append("\n);\n");
            }
        } catch (IOException ioe) {
        }

        return model;
    }

    public static void main(String[] _args_) throws IOException {
        createFolderTree();
        Seconds step1OptTime = Seconds.ZERO;
        Seconds step1EnumTime = Seconds.ZERO;
        Seconds step2OptTime = Seconds.ZERO;

        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");
        int Eb = Integer.parseInt(args.getOrDefault("Eb", "0"));
        int Ed = Integer.parseInt(ArgumentsKt.expectArgument(args, "Ed"));
        int Ef = Integer.parseInt(args.getOrDefault("Ef", "0"));
        int Nr = Eb + Ed + Ef;
        int expectedClusterSize = Integer.parseInt(args.getOrDefault("ClusterSizeApprox", "0"));

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addTerminal();
        LoggerKt.getLogger().addLogFile(format("logs/warp-time-complexity-%d-%d-%d.log", Eb, Ed, Ef), Level.ALL);

        MznSolver step1OptSolver;
        switch (args.get("SolverStep1Opt")) {
            case "Picat":
                LoggerKt.getLogger().info("Selected solver for Step1 Opt = Picat");
                // Indique à Java où est l'exécutable de Minizinc pour convertir les mzn en fzn
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1OptSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                break;
            default:
                LoggerKt.getLogger().error("You must select a Step1 Opt Solver between Picat and Gurobi");
                return;
        }

        MznSolver step1EnumSolver;
        switch (args.get("SolverStep1Enum")) {
            case "Picat":
                LoggerKt.getLogger().info("Selected solver for Step1 Enum = Picat");
                // Indique à Java où est l'exécutable de Minizinc pour convertir les mzn en fzn
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1EnumSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                break;
            default:
                LoggerKt.getLogger().info("No Solver Step1 Enum is given. Using Solver Step1 Opt as Solver Step1 Enum.");
                return;
        }

        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("Eb", Eb);
        parameters.put("Ed", Ed);
        parameters.put("Ef", Ef);
        parameters.put("cluster_size_approximation", expectedClusterSize);

        LoggerKt.getLogger().log("User parameters: %s", parameters);

        int obj;
        if (!args.containsKey("ObjStep1")) {
            LoggerKt.getLogger().info("No ObjStep1 is given, starting Step1Opt for Nr = " + Nr);
            Instant start = Instant.now();
            Integer objStep1 = runStep1Opt(Eb, Ed, Ef, parameters, step1OptSolver);
            LoggerKt.getLogger().log("Time to solve Step1Opt = %s", secondsElapsedSince(start));
            step1OptTime = step1OptTime.plus(secondsElapsedSince(start));
            if (objStep1 == null) {
                LoggerKt.getLogger().warn("No solution was found");
                return;
            } else {
                obj = objStep1;
            }

        } else {
            obj = Integer.parseInt(args.get("ObjStep1"));
            if (obj < 0) {
                obj = -obj;
            }
        }
        LoggerKt.getLogger().info("ObjStep1 = " + obj);

        int lb = obj;
        parameters.put(OBJECTIVE, lb);
        int ub = Integer.MAX_VALUE;

        MznModel.PartialSearch step1Enum = STEP1_ENUM(Eb, Ed, Ef);
        Iterator<MznSolution> iterator = step1EnumSolver.enumerate(step1Enum, parameters);
        int counter = 0;

        int LIMIT = -1;
        if (args.containsKey("Limit")) {
            LIMIT = Integer.parseInt(args.get("Limit"));
            LoggerKt.getLogger().info("User Limit = " + LIMIT);
        }

        while (lb < ub) {
            Instant step1Start = Instant.now();
            LoggerKt.getLogger().log("Searching new Step1 solution for with UB = 2^{%d}, LB = 2^{%d}", ub, lb);
            if ((LIMIT < 0 || (counter < LIMIT)) && iterator.hasNext()) {
                MznSolution solution = iterator.next();
                LoggerKt.getLogger().log("Step 1 solution found (%s)", secondsElapsedSince(step1Start));
                step1EnumTime = step1EnumTime.plus(secondsElapsedSince(step1Start));

                Step1Solution step1Solution = new ParseMznStep1Solution(solution).parseSolution(Eb, Ed, Ef);

                Step2ModelWithStep3 step2 = new Step2ModelWithStep3(
                        Eb, Ed, Ef, App.SBOXES, ub, expectedClusterSize,
                        step1Solution.ΔXupper, step1Solution.freeXupper, step1Solution.freeSXupper,
                        step1Solution.ΔXlower, step1Solution.freeXlower, step1Solution.freeSXlower,
                        step1Solution.knownUpper
                );

                Solver s = step2.model.getSolver();
                Instant step2Start = Instant.now();
                LoggerKt.getLogger().trace("Searching new best warp.Step2 solution");
                while (s.solve()) {
                    if (step2.timeComplexity.getValue() < ub) {
                        ub = step2.timeComplexity.getValue();
                    }
                    LoggerKt.getLogger().log("Step 2 solution found");
                    LoggerKt.getLogger().log("Probability = 2^{-%d} (%s)", step2.objective.getValue(), secondsElapsedSince(step2Start));
                    LoggerKt.getLogger().log("Data complexity = 2^{%d}", step2.dataComplexity.getValue());
                    LoggerKt.getLogger().log("Time complexity = 2^{%d}", step2.timeComplexity.getValue());
                    LoggerKt.getLogger().log("Attack III stage 1 = 2^{%d}", step2.attack_III_stage_1.getValue());
                    LoggerKt.getLogger().log("Attack III stage 2 = 2^{%d}", step2.attack_III_stage_2.getValue());
                    LoggerKt.getLogger().log("rb = %d", step2.rb);
                    LoggerKt.getLogger().log("rf = %d", step2.rf);
                    LoggerKt.getLogger().log("mb = %d", step2.mb);
                    LoggerKt.getLogger().trace("Writing solution to solutions/step1-time-complexity/warp-%d-%d-%d.tex", Eb, Ed, Ef);
                    SolutionWriterKt.buildLatex(Eb, Ed, Ef, step1Solution).writeTo(format("solutions/step1-time-complexity/warp-%d-%d-%d.tex", Eb, Ed, Ef));
                    LoggerKt.getLogger().trace("Writing solution to solutions/step2-time-complexity/warp-%d-%d-%d.tex", Eb, Ed, Ef);
                    SolutionWriterKt.buildLatex(Nr, step2).writeTo(format("solutions/step2-time-complexity/warp-%d-%d-%d.tex", Eb, Ed, Ef));
                    LoggerKt.getLogger().trace("Writing solution to solutions/step1-time-complexity/warp-%d-%d-%d.bin", Eb, Ed, Ef);
                    try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step1-time-complexity/warp-%d-%d-%d.bin", Eb, Ed, Ef)))) {
                        oss.writeObject(step1Solution);
                    }
                    LoggerKt.getLogger().trace("Writing solution to solutions/step2-time-complexity/warp-%d-%d-%d.bin", Eb, Ed, Ef);
                    try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step2-time-complexity/warp-%d-%d-%d.bin", Eb, Ed, Ef)))) {
                        oss.writeObject(new Step2(step2));
                    }

                    int[] gamma = Arrays.stream(step2.δXupper[0]).mapToInt(IntVar::getValue).toArray();
                    int[] delta = Arrays.stream(step2.δXlower[Nr]).mapToInt(IntVar::getValue).toArray();
                    SolutionCheckerKt.writeTo(Nr, gamma, delta, format("solutions/verif-time-complexity/warp-%d-%d-%d.rs", Eb, Ed, Ef));

                    StringBuilder str = new StringBuilder();

                    LoggerKt.getLogger().info("------+-".repeat(16) + " < Eb" + ((Eb == 0) ? " + Ed" : ""));

                    for (int i = 0; i < Nr; i++) {
                        str.setLength(0);
                        for (int j = 0; j < 16; j++) {
                            if (step1Solution.tables[i][j] != null) {
                                str.append(format("%6s| ", step1Solution.tables[i][j]));
                            } else {
                                str.append("      | ");
                            }
                        }
                        LoggerKt.getLogger().info(str.toString());
                        String endLine = "";
                        if (i == Eb - 1) {
                            endLine = " < Ed";
                        } else if (i == Eb + Ed - 1) {
                            endLine = " < Ef";
                        }
                        LoggerKt.getLogger().info("------+-".repeat(16) + endLine);
                    }

                    step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
                    step2Start = Instant.now();
                }
                LoggerKt.getLogger().log("No more Step 2 solution (%s)", secondsElapsedSince(step2Start));
                step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
                counter += 1;
            } else {
                if ((LIMIT > 0 && counter == LIMIT)) {
                    LoggerKt.getLogger().log("Step 1 iteration limit reached for lb = %d. Upgrade lb = %d", lb, (lb + 1));
                } else {
                    LoggerKt.getLogger().log("No Step 1 solution was found. Upgrade lb = " + (lb + 1));
                }
                step1EnumTime = step1EnumTime.plus(secondsElapsedSince(step1Start));
                LoggerKt.getLogger().log("Time to end Step 1 search for lb = %d, %s", lb, secondsElapsedSince(step1Start));
                counter = 0;
                lb += 1;
                parameters.put(OBJECTIVE, lb);
                iterator = step1EnumSolver.enumerate(step1Enum, parameters);
            }
        }

        LoggerKt.getLogger().log("Step1OptTime\tStep1EnumTime\tStep2OptTime\tBest time complexity");
        LoggerKt.getLogger().log("%s\t%s\t%s\t2^{%d}", step1OptTime, step1EnumTime, step2OptTime, ub);

    }

    public static Integer runStep1Opt(int Eb, int Ed, int Ef, HashMap<String, Object> parameters, MznSolver solver) {
        MznSolution step1OptSol = solver.optimize(STEP1_OPT(Eb, Ed, Ef), parameters);
        if (step1OptSol == null) return null;

        String[] lines = step1OptSol.getContent().split("\n");
        for (String line : lines) {
            if (line.startsWith("% obj = ")) {
                return Integer.parseInt(line.substring("% obj = ".length()));
            } else if (line.startsWith(OBJECTIVE + " = ")) {
                return Integer.parseInt(line.substring((OBJECTIVE + " = ").length(), line.lastIndexOf(';')));
            }
        }

        throw new RuntimeException(format("Cannot parse %s from the model", OBJECTIVE));
    }
}
