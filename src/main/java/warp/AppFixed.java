package warp;

import com.github.phd.core.mzn.MznModel;
import com.github.phd.core.mzn.MznSolution;
import com.github.phd.core.mzn.MznSolver;
import com.github.phd.core.utils.ArgumentsKt;
import com.github.phd.core.utils.Logger;
import com.github.phd.core.utils.LoggerKt;
import com.github.phd.infra.MiniZincBinary;
import com.github.phd.infra.PicatBinary;
import org.chocosolver.solver.Solver;
import utils.SolutionWriterKt;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static warp.App.*;

/*
    La recherche est supprimée des fichiers mzn et est ajoutée automatiquement par le code java ci-dessous.
    Ça permet d'avoir le même code pour la step1 opt et la step1 enum.

    Le dossier mzn_models contient trois fichiers:
    - WARP_Step1_Boomerang.mzn : le fichier contenant la step 1 à prorpement parler
    - WARP_Step1_Boomerang.optimized.mzn : le fichier contenant la step 1 à prorpement parler avec améliorations
    - WARP_Step1_Boomerang_decision_vars.mzn : le fichier contenant les variables pour l'heuristique de recherche
    - WARP_Step1_Boomerang_forbid_solution.mzn : le fichier donnant la contraint interdisant la solution précédente
 */
public class AppFixed {

    private static final Config config = new Config("config.txt");

    public static void main(String[] _args_) throws IOException {
        createFolderTree();
        Seconds step1OptTime = Seconds.ZERO;
        Seconds step1EnumTime = Seconds.ZERO;
        Seconds step2OptTime = Seconds.ZERO;

        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");
        int Nr = parseInt(ArgumentsKt.expectArgument(args, "Nr"));

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addTerminal();

        MznSolver step1EnumSolver;
        switch (args.get("SolverStep1Enum")) {
            case "Picat":
                LoggerKt.getLogger().info("Selected solver for Step1 Enum = Picat");
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1EnumSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                break;
            default:
                LoggerKt.getLogger().fatal("No Solver Step1 Enum is given.");
                return;
        }

        int objStep1 = parseInt(args.get("ObjStep1"));
        if (objStep1 < 0) {
            objStep1 = -objStep1;
        }
        LoggerKt.getLogger().info("ObjStep1 = 2^{-%d}", objStep1);

        int objStep2 = parseInt(args.get("ObjStep2"));
        if (objStep2 < 0) {
            objStep2 = -objStep2;
        }
        LoggerKt.getLogger().info("ObjStep2 = 2^{-%d}", objStep2);

        int LIMIT = parseInt(args.get("Limit"));
        LoggerKt.getLogger().info("Limit = %d", LIMIT);

        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("Nr", Nr);
        parameters.put("obj", objStep1);
        MznModel.PartialSearch step1Enum = STEP1_ENUM(Nr);
        Iterator<MznSolution> iterator = step1EnumSolver.enumerate(step1Enum, parameters);

        int counter = 0;
        int uuid = 0;

        Instant step1Start = Instant.now();
        LoggerKt.getLogger().log("Searching new Step1 solution for with UB = 2^{-%d}", objStep1);
        while ((LIMIT < 0 || (counter < LIMIT)) && iterator.hasNext()) {
            MznSolution solution = iterator.next();
            LoggerKt.getLogger().log("Step 1 solution found (%s)", secondsElapsedSince(step1Start));
            step1EnumTime = step1EnumTime.plus(secondsElapsedSince(step1Start));

            Step1Solution step1Solution = new ParseMznStep1Solution(solution).parseSolution(0, Nr, 0);

            Step2Model step2 = new Step2Model(
                    Nr, SBOXES, 256,
                    step1Solution.ΔXupper, step1Solution.freeXupper, step1Solution.freeSXupper,
                    step1Solution.ΔXlower, step1Solution.freeXlower, step1Solution.freeSXlower
            );
            step2.model.arithm(step2.objective, "=", objStep2).post();

            Solver s = step2.model.getSolver();
            Instant step2Start = Instant.now();
            LoggerKt.getLogger().trace("Searching new best warp.Step2 solution");
            if (s.solve()) {
                LoggerKt.getLogger().log("Step 2 solution found. Probability = 2^{-%d} (%s)", step2.objective.getValue(), secondsElapsedSince(step2Start));
                LoggerKt.getLogger().trace("Writing solution to solutions/step2_cluster/warp-%d_%d.tex", Nr, uuid);
                SolutionWriterKt.buildLatex(Nr, step2).writeTo(format("solutions/step2_cluster/warp-%d_%d.tex", Nr, uuid));
                LoggerKt.getLogger().trace("Writing solution to solutions/step1_cluster/warp-%d_%d.bin", Nr, uuid);
                try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step1_cluster/warp-%d_%d.bin", Nr, uuid)))) {
                    oss.writeObject(step1Solution);
                }
                LoggerKt.getLogger().trace("Writing solution to solutions/step2_cluster/warp-%d_%d.bin", Nr, uuid);
                try (ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream(format("solutions/step2_cluster/warp-%d_%d.bin", Nr, uuid)))) {
                    oss.writeObject(new Step2(step2));
                }

                StringBuilder str = new StringBuilder();
                for (int i = 0; i < Nr; i++) {
                    str.setLength(0);
                    for (int j = 0; j < 16; j++) {
                        if (step1Solution.tables[i][j] != null) {
                            str.append(format("%6s| ", step1Solution.tables[i][j]));
                        } else {
                            str.append("      | ");
                        }
                    }
                    LoggerKt.getLogger().info(str.toString());
                    LoggerKt.getLogger().info("------+-".repeat(16));
                }

                step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
                step2Start = Instant.now();
                uuid += 1;
            }
            step2OptTime = step2OptTime.plus(secondsElapsedSince(step2Start));
            step1Start = Instant.now();
            counter += 1;
        }

        LoggerKt.getLogger().log("Step1OptTime\tStep1EnumTime\tStep2OptTime\tBest proba");
        LoggerKt.getLogger().log("%s\t%s\t%s\t2^{-%d}", step1OptTime, step1EnumTime, step2OptTime);

    }

}
