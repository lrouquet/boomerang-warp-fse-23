const BR: usize = 32;
const BR_HALF: usize = 16;
const MAX_NR: usize = 41;
const NR: usize = %NR%;
const NB_KEYS: usize = 16;
const NB_TRIES: usize = 1 << 30;

const SBOX: [u8; BR_HALF] = [ 0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6 ];
const PERM: [usize; BR] = [ 31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10, 15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26 ];
const RC0: [u8; MAX_NR] = [0x0, 0x0, 0x1, 0x3, 0x7, 0xf, 0xf, 0xf, 0xe, 0xd, 0xa, 0x5, 0xa, 0x5, 0xb, 0x6, 0xc, 0x9, 0x3, 0x6, 0xd, 0xb, 0x7, 0xe, 0xd, 0xb, 0x6, 0xd, 0xa, 0x4, 0x9, 0x2, 0x4, 0x9, 0x3, 0x7, 0xe, 0xc, 0x8, 0x1, 0x2];
const RC1: [u8; MAX_NR] = [0x4, 0xc, 0xc, 0xc, 0xc, 0xc, 0x8, 0x4, 0x8, 0x4, 0x8, 0x4, 0xc, 0x8, 0x0, 0x4, 0xc, 0x8, 0x4, 0xc, 0xc, 0x8, 0x4, 0xc, 0x8, 0x4, 0x8, 0x0, 0x4, 0x8, 0x0, 0x4, 0xc, 0xc, 0x8, 0x0, 0x0, 0x4, 0x8, 0x4, 0xc];

fn sboxkey(m: &mut [u8; BR_HALF], k: &[u8; BR], r: usize) {
	for i in 0..BR_HALF {
		m[i] = SBOX[m[i] as usize] ^ k[(r % 2) * 16 + i];
	}
}

fn enc(state: &mut [u8; BR], k: &[u8; BR]) {
	let mut temp = [0; BR_HALF];
	for i in 0..NR {
		for j in 0..BR_HALF {
			temp[j] = state[j * 2];
		}
		sboxkey(&mut temp, k, i);
		for j in 0..BR_HALF {
			state[2 * j + 1] = state[2 * j + 1] ^ temp[j];
		}
		state[1] = state[1] ^ RC0[i];
        state[3] = state[3] ^ RC1[i];
		permutation(state);
	}
}

fn dec(state: &mut [u8; BR], k: &[u8; BR]) {
	let mut temp = [0; BR_HALF];
	for i in 0..NR {
		permutation_inv(state);
        state[1] = state[1] ^ RC0[NR - i - 1];
        state[3] = state[3] ^ RC1[NR - i - 1];
		for j in 0..BR_HALF {
			temp[j] = state[j * 2];
		}
		sboxkey(&mut temp, k, NR - i - 1);
		for j in 0..BR_HALF {
			state[2 * j + 1] = state[2 * j + 1] ^ temp[j];
		}
	}
}

fn permutation(state: &mut [u8; BR]) {
	let temp = state.clone();
	for j in 0..BR {
		state[PERM[j]] = temp[j];
	}
}

fn permutation_inv(state: &mut [u8; BR]) {
	let temp = state.clone();
	for j in 0..BR {
		state[j] = temp[PERM[j]];
	}
}

fn main() {

	let mut rand = Rand::new(0);
	#[allow(non_snake_case)]
    let mut FREE = || {
        let mut tmp = rand.int64() as u8 & 0xf;
        while tmp == 0 {
            tmp = rand.int64() as u8 & 0xf
        }
        tmp
    };

	let gamma = [ %GAMMA% ];
	let delta = [ %DELTA% ];

	let mut m = [0; BR];
	let mut m1 = [0; BR];

	let mut key = [0u8; BR];
	let mut total = 0;
	for key_index in 0..NB_KEYS {
		for ta in 0..BR {
			key[ta] = (rand.int64() ^ ta ^ key_index) as u8 & 0xf;
		}

		let mut cpt = 0;
		for _attempt in 0..NB_TRIES {
			for ta in 0..BR {
				m[ta] = (rand.int64() ^ ta ^ key_index) as u8 & 0xf;
				m1[ta] = m[ta] ^ gamma[ta];
			}

			enc(&mut m, &key);
			enc(&mut m1, &key);

			for ta in 0..BR {
				m[ta] ^= delta[ta];
				m1[ta] ^= delta[ta];
			}

			dec(&mut m, &key);
			dec(&mut m1, &key);

			let mut is_valid = true;
			for i in 0..BR {
				if m[i] ^ m1[i] != gamma[i] {
					is_valid = false;
					break;
				}
			}

			if is_valid {
				cpt += 1;
			}
		}
		println!("Boomerang: {}/{}, proba = 2^{{{:.4}}}", cpt, NB_TRIES, (cpt as f64 / NB_TRIES as f64).log2());
		total += cpt;
	}
    println!("Mean: {:.2}/{}, proba = 2^{{{:.4}}}", total as f64 / NB_KEYS as f64, NB_TRIES, (total as f64 / (NB_TRIES * NB_KEYS) as f64).log2());

}

struct Rand {
	v: usize
}

impl Rand {
	fn new(seed: usize) -> Rand {
		let mut rand = Rand { v: 4101842887655102017 };
		rand.v ^= seed;
		rand.v = rand.int64();
		rand
	}

	fn int64(&mut self) -> usize {
		self.v ^= self.v >> 21;
		self.v ^= self.v << 35;
		self.v ^= self.v >> 4;
		let (res, _overflow) = self.v.overflowing_mul(2685821657736338717);
		res
	}
}