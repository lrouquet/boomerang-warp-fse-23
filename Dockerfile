FROM ubuntu:20.04

# Defining default non-root user UID, GID, and name
ARG USER_UID="1000"
ARG USER_GID="1000"
ARG USER_NAME="default"

# Creating default non-user
RUN groupadd -g $USER_GID $USER_NAME\
    && useradd -m -g $USER_GID -u $USER_UID $USER_NAME

# Installing basic packages
RUN apt-get update\
    && apt-get install -y zip unzip curl

# Switching to non-root user to install SDKMAN!
USER $USER_UID:$USER_GID

# Downloading SDKMAN!
RUN curl -s "https://get.sdkman.io" | bash

# Installing Java and Gradle
ARG JAVA_VERSION="11.0.12-open"
ARG GRADLE_VERSION="6.8"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install java $JAVA_VERSION\
    && sdk install gradle $GRADLE_VERSION"
ENV GRADLE_HOME="/home/default/.sdkman/candidates/gradle/current" 
ENV JAVA_HOME="/home/default/.sdkman/candidates/java/current" 

# Installing MiniZinc
ARG MINIZINC_VERSION="2.5.5"
RUN cd /home/default\
    && curl -L "https://github.com/MiniZinc/MiniZincIDE/releases/download/${MINIZINC_VERSION}/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz" -o MiniZinc.tgz\
    && tar -xzf MiniZinc.tgz\
    && rm MiniZinc.tgz
ENV MINIZINC_HOME="/home/default/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64"

# Installing Picat
ARG PICAT_VERSION="30_5"
RUN cd /home/default\
    && curl -L "http://picat-lang.org/download/picat${PICAT_VERSION}_linux64.tar.gz" -o Picat.tgz\
    && tar -xzf Picat.tgz\
    && rm Picat.tgz\
    && cd Picat/lib\
    && curl -L http://picat-lang.org/flatzinc/fzn_picat_sat.pi -o fzn_picat_sat.pi\
    && curl -L http://picat-lang.org/flatzinc/fzn_parser.pi -o fzn_parser.pi\
    && curl -L http://picat-lang.org/flatzinc/fzn_tokenizer.pi -o fzn_tokenizer.pi
ENV PICAT_HOME="/home/default/Picat"

ENV PATH="$PICAT_HOME:$MINIZINC_HOME/bin:$GRADLE_HOME/bin:$JAVA_HOME/bin:$PATH"
