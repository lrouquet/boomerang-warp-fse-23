- [x] Tester sur 22 tours.

- [x] Découpage 8-6-8 => proba meilleur que celle trouvée par le modèle. 9ième tour en partant du haut et 9ième tour en partant du bas.

$\approx 2^{16}$ solutions envisagées.

Essayé sur 23 tours ? $2^{12}$ solutions pour passer.

## Todo

Essayer de configurer une step1 en fonction d'une autre step 1 donnée pour le calcul des clusteurs.

## Notes

### 28 Sep.

Calcul des *clusters* sur **une seule** Step1 (même chemin différentiel) avec 2 modes.

- Mode 1 "Automatique", `AppCluster` choisit automatiquement le début de $r$ en fonction des *DDT$^2$*

- Mode 2 "Manuel", `AppCluster` l'utilisateur indique le découpage de $r$ à la main.

### 29 Sep.

Calculer des *clusters* avec **plusieurs** Step1 (différent chemins différentiels). On garde les mêmes mode que pour le `28 Sep`. Cependant il est maintenant possible d'utiliser plusieurs Step 1.

Pour effectuer le calcul, on récupère la Step2 Opt ainsi que la Step 1 qui lui correspond. Je crée un modèle Minizinc en ajoutant des contraintes pour que : 

$$
\Delta Xupper[\mathtt{start}] = \mathtt{step1.}\Delta X upper[\mathtt{start}] \\ \text{ et } \\
\Delta Xlower[\mathtt{end}] = \mathtt{step1}.\Delta Xlower[\mathtt{end}]
$$

On ajoute une limite arbitraire afin d'éliminer les solutions Step 1 trop loin de l'optimal (probabilité trop basse). Ensuite on énumère les solutions Step1 qui correspondent à la solution Step 1 initiale et pour chaque solution Step 1 valide on énumère les solutions Step 2 qui vérifient : 

$$
\delta Xupper[\mathtt{start}] = \mathtt{step2.}\delta X upper[\mathtt{start}] \\ \text{ et } \\
\delta Xlower[\mathtt{end}] = \mathtt{step2.}\delta Xlower[\mathtt{end}]
$$

On effectue enfin le décompte les probabilités.

Pour que les Step1 soient différentes nous ajoutons la contrainte qui indique qu'il faut qu'il y ait au moins une boîte S différentes dans les tours "`mid`".

```minizinc
constraint not(
    (forall(j in BR_HALF, r in START..END_INCLUSIVE) (DXupper[r, j] = DXupper_%SOL%[r, j])) /\
    (forall(j in BR_HALF, r in START..END_INCLUSIVE) (DXlower[r, j] = DXlower_%SOL%[r, j]))
);
```