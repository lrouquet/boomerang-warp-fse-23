int: Eb;
int: Ed;
int: Ef;

int: Nr = Eb + Ed + Ef;

function set of int: range(int: start, int: exclusive_end) = start..exclusive_end - 1;

set of int: ROUNDS = range(0, Nr + 1);
set of int: ATTACK_ROUNDS = range(0, Nr);
set of int: BEGIN_ROUNDS = range(0, Eb);
set of int: DISTINGUISHER_ROUNDS = range(Eb, Eb + Ed);
set of int: FINISH_ROUNDS = range(Eb + Ed, Nr);

set of int: BR = range(0, 32);
set of int: BR_HALF = range(0, 16);

var int: obj; % 2^{-0} to 2^{-256}
var int: internal_obj; % 2^{-0} to 2^{-256}

array [BR] of int: PERM = array1d(BR, [ 31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10, 15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26 ]);
array [BR_HALF] of var int: Pi_even;
array [BR_HALF] of var int: Pi_odd;
constraint forall(col in BR_HALF) (
    Pi_even[col] = PERM[2 * col] /\
    Pi_odd[col]  = PERM[2 * col + 1]
);

%%% Variables %%%
array [ROUNDS, BR] of var bool: DXupper;
array [ROUNDS, BR] of var bool: FreeXupper;
array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBupper;

array [ROUNDS, BR] of var bool: DXlower;
array [ROUNDS, BR] of var bool: FreeXlower;
array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBlower;

%%% At least one non-zero differential at first round
constraint sum(col in BR) (DXupper[0, col]) != 0;
constraint sum(col in BR) (DXlower[0, col]) != 0;

function var bool: isTable(int: round, int: col) = (
    (FreeSBupper[round, col] /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col])) \/
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ FreeSBlower[round, col]) \/
    (not(DXupper[round, 2 * col]) /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col])) \/
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ not(DXlower[round, 2 * col])) \/
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ not(FreeXlower[round, 2 * col]))
);

% difference specified in one characteristic (input and output), and unspecified difference in the other characteristic
function var bool: isDDT2(int: round, int: col) = (
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ DXlower[round, 2 * col] /\ FreeXlower[round, 2 * col] /\ FreeSBlower[round, col]) \/
    (DXupper[round, 2 * col] /\ FreeXupper[round, 2 * col] /\ FreeSBupper[round, col] /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col]))
);

%%%%%%%%%%%%%%%%%%%%%%%% UPPER TRAIL
%nibbles that are kept (even ones)
constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    (DXupper[r + 1, Pi_even[col]] = DXupper[r, 2 * col]) /\
    (FreeXupper[r + 1, Pi_even[col]] = FreeXupper[r, 2 * col])
);

%%% contraintes sur Sb et sur Xored (odd ones)
constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    XOR(DXupper[r + 1, Pi_odd[col]], DXupper[r, 2 * col + 1], DXupper[r, 2 * col]) /\
    FreeXupper[r + 1, Pi_odd[col]] = (FreeXupper[r, 2 * col + 1] \/ FreeSBupper[r, col])
);

%%%%%%%%%%%%%%%%%%%%%%%% LOWER TRAIL
%%% contraintes sur Sb et sur Xored (odd ones)
constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    (DXlower[r, 2 * col] = DXlower[r + 1, Pi_even[col]]) /\
    (FreeXlower[r, 2 * col] = FreeXlower[r + 1, Pi_even[col]])
);

constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    XOR(DXlower[r + 1, Pi_odd[col]], DXlower[r, 2 * col + 1], DXlower[r, 2 * col]) /\
    FreeXlower[r, 2 * col + 1] = (FreeXlower[r + 1, Pi_odd[col]] \/ FreeSBlower[r, col])
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%XOR
predicate XOR(var 0..1: A, var 0..1: B, var 0..1: C) = A + B + C != 1;

%%%%%%%%%%MILIEU%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
constraint forall(round in DISTINGUISHER_ROUNDS, col in BR_HALF) (
    one_objective_constraint(
        DXupper[round, 2 * col], FreeXupper[round, 2 * col], FreeSBupper[round, col],
        DXlower[round, 2 * col], FreeXlower[round, 2 * col], FreeSBlower[round, col]
    )
);

predicate one_objective_constraint(
var bool: DXupper, var bool: freeXupper, var bool: freeSBupper,
var bool: DXlower, var bool: freeXlower, var bool: freeSBlower
) = (
    ( not(freeXupper) /\ not(freeSBupper) /\ not(freeXlower) /\ not(freeSBlower) ) \/
    ( not(freeXupper) /\ not(freeSBupper) /\ DXlower /\ freeSBlower ) \/
    ( DXupper /\ not(freeXupper) /\ DXlower /\ not(freeXlower) ) \/
    ( DXupper /\ freeSBupper /\ not(freeXlower) /\ not(freeSBlower) )
);

array [DISTINGUISHER_ROUNDS] of var int: actives_upper;
constraint forall(round in DISTINGUISHER_ROUNDS) (
  sum(col in BR_HALF) (DXupper[round, 2 * col]) = actives_upper[round]
);
array [DISTINGUISHER_ROUNDS] of var int: actives_lower;
constraint forall(round in DISTINGUISHER_ROUNDS) (
  sum(col in BR_HALF) (DXlower[round, 2 * col]) = actives_lower[round]
);

constraint sum(round in DISTINGUISHER_ROUNDS, col in BR_HALF) (isTable(round, col) + isDDT2(round, col)) = internal_obj;
constraint obj = 2 * internal_obj;

predicate maximum(var int: x, array[int] of var int: arr) = forall(el in arr) (x >= el) /\ exists(el in arr) (x == el);
predicate minimum(var int: x, array[int] of var int: arr) = forall(el in arr) (x <= el) /\ exists(el in arr) (x == el);

function var int: log(var int: pow_k, int: k) = let {
  var 0..10: res;
  constraint pow(k, res) = pow_k;
} in res;

function var int: floor_log(var int: pow_k, int: k) = let {
  var 0..10: res;
  constraint if pow_k == 1 then
    res = 0
  else
    (pow(k, res) <= pow_k) /\ (pow_k < pow(k, res + 1))
  endif;
} in res;

function var int: ceil_log(var int: pow_k, int: k) = let {
  var 0..10: res;
  constraint if pow_k == 1 then
    res = 0
  else
    (pow(k, res - 1) < pow_k) /\ (pow_k <= pow(k, res))
  endif;
} in res;

function var int: max(array [int] of var int: arr) = let { var int: max_element; constraint forall(el in arr) (max_element >= el) /\ exists(el in arr) (max_element == el); } in max_element;

array [0..19] of var int: MINIMAL_N_SBOXES = array1d(0..19, [0, 0, 1, 2, 3, 4, 5, 6, 11, 14, 17, 22, 28, 34, 40, 47, 52, 57, 61, 66]);

constraint forall(i in 0..min(18, Eb + Ed)) (
    sum(r in Eb..Eb + i, col in BR_HALF) (DXupper[r, 2 * col]) >= MINIMAL_N_SBOXES[i + 1]
);

constraint forall(i in 0..min(18, Eb + Ed)) (
    sum(r in Eb + Ed - i..Eb + Ed, col in BR_HALF) (DXlower[r, 2 * col]) >= MINIMAL_N_SBOXES[i + 1]
);

