int: Nr;

set of int: ROUNDS = 0..Nr;
set of int: ATTACK_ROUNDS = 0..Nr - 1;

set of int: BR = 0..31;
set of int: BR_HALF = 0..15;

var int: obj; % 2^{-0} to 2^{-256}

array [BR] of int: PERM = array1d(BR, [ 31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10, 15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26 ]);
array [BR_HALF] of var int: Pi_even;
array [BR_HALF] of var int: Pi_odd;
constraint forall(col in BR_HALF) (
    Pi_even[col] = PERM[2 * col] /\
    Pi_odd[col]  = PERM[2 * col + 1]
);

%%% Variables %%%
array [ROUNDS, BR] of var bool: DXupper;
array [ROUNDS, BR] of var bool: FreeXupper;
array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBupper;

array [ROUNDS, BR] of var bool: DXlower;
array [ROUNDS, BR] of var bool: FreeXlower;
array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBlower;

%%% At least one non-zero differential at first round
constraint sum(col in BR) (DXupper[0, col]) != 0;
constraint sum(col in BR) (DXlower[0, col]) != 0;

% constraint forall(i in 0..13, j in BR) ( DXupper[i, j] = DXupperSol[i, j] );
% constraint forall(i in 8..21, j in BR) ( DXlower[i, j] = DXlowerSol[i, j] );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TABLES
% difference specified in one characteristic (input and output), and zero in the other characteristic
function var bool: isDDT(int: round, int: col) = (
    (not(DXupper[round, 2 * col]) /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col])) \/
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ not(DXlower[round, 2 * col]))
);
% input difference specified in the upper characteristic, output difference specified in the lower characteristic, and other differences unspecified.
function var bool: isFBCT(int: round, int: col) = (
    DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ FreeSBupper[round, col] /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ FreeSBlower[round, col]
);
% difference specified in one characteristic (input and output), and unspecified difference in the other characteristic
function var bool: isDDT2(int: round, int: col) = (
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ DXlower[round, 2 * col] /\ FreeXlower[round, 2 * col] /\ FreeSBlower[round, col]) \/
    (DXupper[round, 2 * col] /\ FreeXupper[round, 2 * col] /\ FreeSBupper[round, col] /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col]))
);
% input and output difference specified in the upper characteristic, output difference specified in the lower characteristic (and input unspecified).
function var bool: isFBDT(int: round, int: col) = (
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ FreeSBlower[round, col]) \/
    (DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col])  /\ FreeSBupper[round, col] /\ DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col]))
);

% all differences specified.
function var bool: isFBET(int: round, int: col) = (
    DXupper[round, 2 * col] /\ not(FreeXupper[round, 2 * col]) /\ not(FreeSBupper[round, col]) /\
    DXlower[round, 2 * col] /\ not(FreeXlower[round, 2 * col]) /\ not(FreeSBlower[round, col])
);

%%%%%%%%%%%%%%%%%%%%%%%% UPPER TRAIL
%nibbles that are kept (even ones)
constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    (DXupper[r + 1, Pi_even[col]] = DXupper[r, 2 * col]) /\
    (FreeXupper[r + 1, Pi_even[col]] = FreeXupper[r, 2 * col])
);

%%% contraintes sur Sb et sur Xored (odd ones)
constraint forall(col in BR_HALF, r in ATTACK_ROUNDS) (
    XOR(DXupper[r + 1, Pi_odd[col]], DXupper[r, 2 * col + 1], DXupper[r, 2 * col]) /\
    FreeXupper[r + 1, Pi_odd[col]] = (FreeXupper[r, 2 * col + 1] \/ FreeSBupper[r, col])
);

%%%%%%%%%%%%%%%%%%%%%%%% LOWER TRAIL
%%% contraintes sur Sb et sur Xored (odd ones)
constraint forall(col in BR_HALF, r in ATTACK_ROUNDS) (
    (DXlower[r, 2 * col] = DXlower[r + 1, Pi_even[col]]) /\
    (FreeXlower[r, 2 * col] = FreeXlower[r + 1, Pi_even[col]])
);

constraint forall(r in ATTACK_ROUNDS, col in BR_HALF) (
    XOR(DXlower[r + 1, Pi_odd[col]], DXlower[r, 2 * col + 1], DXlower[r, 2 * col]) /\
    FreeXlower[r, 2 * col + 1] = (FreeXlower[r + 1, Pi_odd[col]] \/ FreeSBlower[r, col])
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%XOR
predicate XOR(var 0..1: A, var 0..1: B, var 0..1: C) = A + B + C != 1;

%%%%%%%%%%MILIEU%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
constraint forall(round in ATTACK_ROUNDS, col in BR_HALF) (
    one_objective_constraint(
        DXupper[round, 2 * col], FreeXupper[round, 2 * col], FreeSBupper[round, col],
        DXlower[round, 2 * col], FreeXlower[round, 2 * col], FreeSBlower[round, col]
    )
);

predicate one_objective_constraint(
var bool: DXupper, var bool: freeXupper, var bool: freeSBupper,
var bool: DXlower, var bool: freeXlower, var bool: freeSBlower
) = (
    (freeSBupper -> DXupper) /\
    (freeSBlower -> DXlower) /\
    (freeXupper -> freeSBupper)  /\
    (freeXlower -> freeSBlower) /\
    (not(freeXupper) \/ not(freeSBlower)) /\
    (not(freeXlower) \/ not(freeSBupper))
);

constraint sum(round in ATTACK_ROUNDS, col in BR_HALF) (
    isDDT(round, col) * 2 +
    isDDT2(round, col) * 4 +
    % isFBCT(round, col) * 2 +
    isFBET(round, col) * 2 +
    isFBDT(round, col) * 2
) = obj;

output [
    "obj = ", show(obj), ";\n",
    "DXupper = ", show(DXupper), ";\n",
    "FreeXupper = ", show(FreeXupper), ";\n",
    "FreeSBupper = ", show(FreeSBupper), ";\n",
    "DXlower = ", show(DXlower), ";\n",
    "FreeXlower = ", show(FreeXlower), ";\n",
    "FreeSBlower = ", show(FreeSBlower), ";\n",
];
